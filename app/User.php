<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'company_id', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserById($id)
    {
        return DB::table('users')
            ->where('users.id', $id)
            ->leftJoin('company', 'company.creator_id', '=', 'users.id')
            ->first([
                'users.id AS id',
                'users.name AS name',
                'users.company_id AS company_id',
                'users.phone AS phone',
                'users.email AS email',
                'users.created_at AS created_at',
                'users.updated_at AS updated_at',
                'users.status AS status',
                'company.name AS company_name',
                'company.domain AS company_domain',
                'company.name AS company_name'
            ]);
    }

    public function getAll()
    {
        return DB::table('users')
            ->leftJoin('company', 'company.creator_id', '=', 'users.id')
            ->get([
                'users.id AS id',
                'users.name AS name',
                'users.phone AS phone',
                'users.email AS email',
                'users.created_at AS created_at',
                'users.updated_at AS updated_at',
                'users.status AS status',
                'company.name AS company_name'
            ]);
    }

    public function editUser($user, $company_id)
    {
        $user_id = DB::table('user_invite')
            ->where('token', $user['token'])
            ->first(['user_id']);

        DB::table('users')
            ->where('id', $user_id->user_id)
            ->update([
                'name' => $user['name'],
                'phone' => '',
                'company_id' => $company_id,
                'role_id' => 1,
                'password' => Hash::make($user['password']),
            ]);

        return $user_id;
    }
}
