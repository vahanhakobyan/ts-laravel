<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Description extends Model
{
    public function idToTime($ts)
    {
        $hexTs = dechex($ts);
        $hexTs = str_pad($hexTs, 8, "0", STR_PAD_LEFT);

        return $hexTs."0000000000000000";
    }

    protected function getUnique($domain_id)
    {
        $domain = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where(['domain_id' => $domain_id])
            ->select(['_id'])
            ->get();

        if($domain->count() > 890) {
            $job_id = rand(1000, 9999);
        }
        else {
            $job_id = rand(100, 999);
        }

        $get_job_id = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where(['job_id' => $job_id])
            ->select(['_id'])
            ->first();

        if($get_job_id == null) {
            return $job_id;
        }
        else {
            return $this->getUnique($domain_id);
        }
    }

    protected function file_post_contents($url, $data)
    {
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/json',
                'content' => $data
            )
        );

        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
    }

    public function getAll()
    {
        return DB::connection('mongodb')
            ->table('job_descriptions')
            ->select(['date_created', 'is_active', 'email', 'domain_id', 'fields'])
            ->get();
    }

    public function setDescription($seniority, $job_posting_name, $group_name, $group_id)
    {
        $user = Auth::user();
        $domain = DB::table('company')
            ->where('creator_id', $user['id'])
            ->first(['domain']);

        $job_id = $this->getUnique($domain->domain);

        $unique_email = $domain->domain . ".r." . $job_id . "@textslice.com";

        $data_string = json_encode(["email" => $unique_email]);

        $this->file_post_contents('http://textslice.com:5059/api/create_email_alias', $data_string);

        $ex_tx = explode('|', $seniority);

        if (!empty($ex_tx[1])) {
            $fields = [
                'seniority' => trim($ex_tx[1]),
                'position' => $job_posting_name,
                'total_experience' => substr($ex_tx[0], 18)
            ];
        } else {
            $fields = [
                'seniority' => $seniority,
                'position' => $job_posting_name
            ];
        }

        $content = [
            'project_id' => $group_id,
            'domain_id' => $domain->domain,
            'fields' => $fields,
            'creator_id' => $user['id'],
            'date_created' => new \DateTime(),
            'is_active' => true,
            'email' => $unique_email,
            'job_id' => $job_id
        ];

        $id = DB::connection('mongodb')
            ->table('job_descriptions')
            ->insertGetId($content);

        return [
            'email' => $unique_email,
            'id' => $id.'',
            'pid' => $group_id.'',
            'pnm' => $group_name
        ];
    }

    public function editDescription($id)
    {
        $description = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where(['_id' => $id])
            ->first();

        $group = DB::table('groups')
            ->where(['id' => $description['project_id']])
            ->first();

        $job_applications = DB::connection('mongodb')
            ->table('job_applications')
            ->where(['job_description_id' => new \MongoDB\BSON\ObjectID($id)])
            ->get(['_id']);
		$count = count(json_decode($job_applications,true));
        return [
            'description' => $description,
            'project' => $group,
            'job_applications_count' => $count,
            'user' => Auth::user()
        ];
    }

    public function getByGroup($id)
    {
        return DB::connection('mongodb')
            ->table('job_descriptions')
            ->where(['project_id' => $id])
            ->orderByDesc('_id')
            ->get();
    }

    public function updateJP($id, $st)
    {
        $active_forms = [];

        if ($st) {
            foreach ($st as $v) {
                $active_forms[] = substr($v, 3, 1);
            }
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$set' => [
                    'active_forms' => $active_forms
                ]
            ]);

        return 'ok';
    }

    public function updateHI($id, $posting, $level, $total_experience)
    {
        $tx = '-';
        if (!empty($total_experience)) {
            $tx = floatval($total_experience);
            if(!is_numeric($tx)) {
                $tx = '-';
            }
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update(['fields.position' => $posting, 'fields.seniority' => $level, 'fields.total_experience' => $tx]);

        $jd = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->first();

        return [
            'position' => $jd['fields']['position'],
            'seniority' => $jd['fields']['seniority'],
            'total_experience' => $jd['fields']['total_experience']
        ];
    }

    public function getAllApplications($project_id = null, $skill = null, $str = null)
    {
        if(empty($project_id) || is_null($project_id) || $project_id == 0) {
            $project_id = null;
        }

        if(empty($skill) || is_null($skill) || $skill == 'All') {
            $skill = null;
        }

        $query = DB::connection('mongodb')
            ->table('job_descriptions');

        $query->where('creator_id', Auth::id());

        if($project_id) {
            $query->where('project_id', $project_id);
        }

        if($skill) {
            $query->where('fields.skills.skill', $skill);
        }

        if(!empty($str)) {
//            $query->where(
//                '$or' => [
//                    ['fields.position', 'like', '%$str%'],
//                    ['fields.skills.skill', 'like', '%$str%'],
//                    ['fields.other_skills.skill', 'like', '%$str%'],
//                    ['fields.education', 'like', '%$str%'],
//                    ['fields.language', 'like', '%$str%'],
//                    ['fields.questions', 'like', '%$str%'],
//                    ['fields.requirements', 'like', '%$str%']
//                ]
//            );
            $query->where('fields.position', 'like', "%$str%");
        }

        $query->orderByDesc('_id');

        return $query->get();
    }

    public function getResultsForJobPosting($array)
    {
        /*
        $db = $this->connectMD();
        $collection = $db->job_applications;

        $cursor = $collection->find([
            'job_description_id' => ['$in' => $array],
            'data' => ['$exists' => true]
        ], [
            '_id' => 1,
            'job_description_id' => 1,
            'data' => 1,
            'file_type' => 1,
            'date' => 1
        ]);

        return $cursor;
        */
        return DB::connection('mongodb')
            ->table('job_applications')
            //->whereIn('job_description_id', $array)
            ->whereNotNull('data')
            //->whereIn('job_description_id', $array)
            //->where('data', 'exists', true)
            ->get(['_id', 'job_description_id', 'data', 'file_type', 'date']);
    }

    public function getAllJobsCount()
    {
        $user_id = Auth::id();

        $week = time() - 3600 * 24 * 7;

        //$cursor = $collection->find(['_id' => ['$gte' => $this->idToTime($week)], 'creator_id' => new MongoId($_COOKIE['ts_user_id'])])->count();
        //->where(['_id' => ['$gte' => $this->idToTime($week)], 'creator_id' => $user_id])
        return DB::connection('mongodb')
            ->table('job_descriptions')
            ->count();
    }

    public function getAllApplicationsCount()
    {
        $user_id = Auth::id();

//        $db = $this->connectMD();
//        $collection = $db->job_applications;
//        $collection_jd = $db->job_descriptions;
//        $cursor_jd = $collection_jd->find(['creator_id' => new MongoId($_COOKIE['ts_user_id'])], ['_id' => 1]);

        $descriptions = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where(['creator_id' => $user_id])
            ->get(['_id']);

        $array = [];

        foreach ($descriptions as $item) {
            $array[] = $item['_id'].'';
        }

        $week = time() - 3600 * 24 * 7;

        //$cursor = $collection->find(['_id' => ['$gte' => $this->idToTime($week)], 'data' => ['$exists' => true], 'job_description_id' => ['$in' => $array]])->count();
        //->where(['_id' => ['$gte' => $this->idToTime($week)], 'job_description_id' => ['$in' => $array]])
        return DB::connection('mongodb')
            ->table('job_applications')
            ->whereNotNull('data.result_data')
            //->whereDate('date', '>', $week)
            ->count();
    }

    public function getCompanyByUserId()
    {
        $user_id = Auth::id();

        return DB::table('company')
            ->where(['creator_id' => $user_id])
            ->first(['name']);
    }

    public function updateJobPostingSkill($id, $jp1, $sk)
    {
        $skills = [];
        $field = 'fields.skills';

        if ($sk == 'os') {
            $field = 'fields.other_skills';
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$unset' => [
                    $field => 1
                ]
            ]);

        for ($i = 0; $i < sizeof($jp1); $i++) {
            $ex = explode(' / ', strip_tags($jp1[$i]));
            $ex_1 = 'not required';
            switch (trim($ex[1])) {
                case 'not required':
                    $ex_1 = 'not required';
                    break;
                case '< 1 year':
                    $ex_1 = '< 1';
                    break;
                case '1-2 years':
                    $ex_1 = '1-2';
                    break;
                case '2-3 years':
                    $ex_1 = '2-3';
                    break;
                case '3-5 years':
                    $ex_1 = '3-5';
                    break;
                case '5-7 years':
                    $ex_1 = '5-7';
                    break;
                case '7-10 years':
                    $ex_1 = '7-10';
                    break;
                case '> 10 years':
                    $ex_1 = '> 10';
                    break;
            }

            $ex_comma = explode(',', $ex[0]);

            if(is_array($ex_comma) && count($ex_comma) > 1) {
                for($i=0; $i<count($ex_comma); $i++) {
                    if(!empty($ex_comma[$i]) && $ex_comma[$i] != ' ') {
                        $skills[] = [
                            'skill' => trim($ex_comma[$i]),
                            'experience' => trim($ex_1)
                        ];
                    }
                }
            }
            else {
                $skills[] = [
                    'skill' => trim($ex[0]),
                    'experience' => trim($ex_1)
                ];
            }
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$set' => [
                    $field => $skills,
                    'last_updated' => new \DateTime()
                ]
            ]);

        return 'ok';
    }

    public function updateJobPostingEducation($id, $edu)
    {
        $data = [];

        if ($edu) {
            foreach ($edu as $v) {
                $data[] = $v;
            }
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$set' => [
                    'fields.education' => $edu
                ]
            ]);

        return 'ok';
    }

    public function updateJobPostingQR($id, $data5, $data6)
    {
        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$unset' => [
                    'fields.questions' => 1,
                    'fields.requirements' => 1
                ]
            ]);

        $jp5 = [];
        $jp6 = [];

        for ($i = 0; $i < sizeof($data5); $i++) {
            $jp5[] = trim(ltrim($data5[$i], 'Question: '));
        }

        for ($j = 0; $j < sizeof($data6); $j++) {
            $jp6[] = trim($data6[$j]);
        }

        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->update([
                '$set' => [
                    'fields.questions' => $jp5,
                    'fields.requirements' => $jp6
                ]
            ]);

        return 'ok';
    }

    public function summaryReport($jp, $arr)
    {
        $cursor_jp = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $jp)
            ->first();

        $condition = [
            'job_description_id' => $jp
            //'data' => ['$exists' => true]
        ];

        $primary_skills = $all_skills = $txt_search = '';

        if(!empty($data['arr']['PrimarySkills']) && $data['arr']['PrimarySkills'] != 'All') {
            $primary_skills = $condition['data.special_data.ps'] = $data['arr']['PrimarySkills'];
        }

        if(!empty($data['arr']['AllSkills']) && $data['arr']['AllSkills']!='All') {
            $all_skills = $condition['data.result_data.skills.extracted.value'] = $data['arr']['AllSkills'];
        }

        if(!empty($data['arr']['TxtSearch'])) {
            $txt_search = $condition['data.result_data.personal.names.value'] = $data['arr']['TxtSearch'];
        }

        $cursor = DB::connection('mongodb')
            ->table('job_applications')
            //->where('job_description_id', $jp)
            ->orderByDesc('_id')
            ->get(['_id', 'data', 'applicant_email', 'applicant_name', 'file_type', 'hide', 'approved_date', 'date']);

        $cursor_2 = DB::connection('mongodb')
            ->table('job_applications')
            ->where('job_description_id', $jp)
            //->where(['data' => ['$exists' => true]])
            ->orderByDesc('_id')
            ->get(['_id', 'data', 'applicant_email', 'applicant_name', 'file_type', 'hide', 'approved_date', 'date']);

        return [
            'app' => $cursor,
            'app_2' => $cursor_2,
            'jp' => $cursor_jp,
            'primary_skills' => $primary_skills,
            'all_skills' => $all_skills,
            'txt_search' => $txt_search
        ];
    }

    public function removeJobPosting($id)
    {
        DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('_id', $id)
            ->delete();

        return 'ok';
    }

    public function updateJobPosting($id, $txt)
    {
        DB::table('groups')
            ->where('id', $id)
            ->update(['name' => trim($txt)]);

        return 'ok';
    }

    public function loadSelectBoxes()
    {
        $projects = DB::table('groups')
            ->where('id', Auth::id())
            ->orderByDesc('id')
            ->get();

        $result = DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('creator_id', Auth::id())
            ->get(['fields']);

        $skills = [];

        foreach ($result as $item) {
            if (!empty($item['fields']['skills'])) {
                foreach ($item['fields']['skills'] as $skill) {
                    $skills[] = $skill['skill'];
                }
            }
        }

        return [
            'projects' => $projects,
            'skills' => array_unique($skills),
            'skill_status' => count($skills),
            'posted_this_week_jobs' => $this->getAllJobsCount(),
            'posted_this_week_applicants' => $this->getAllApplicationsCount()
        ];
    }
	
	public function getJobDescriptionByEmail($email)
	{
		if(empty($email)){return false;}
		return DB::connection('mongodb')
            ->table('job_descriptions')
            ->where('email', $email)
            ->first();
		
	}
}
