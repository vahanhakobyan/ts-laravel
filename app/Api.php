<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\HelperController;

class Api extends Model
{
    public static function getAll()
    {
        $array = ['1' => 123, '2' => 654];

        return (array)$array;
    }
	
    public function saveNewMail($email_body)
    {
		/////////////TEST
		//$email_body = include 'new_mail.php';
		/////
		$resume = new Resume();
		$description = new Description();
		if(empty($email_body['attachments'])){ return;}
		
		$jobDesc = $description->getJobDescriptionByEmail($email_body['to']['value'][0]['address']);
		if(empty($jobDesc)){ return;}
		$jobFields = json_encode($jobDesc['fields']);
		
		foreach ($email_body['attachments'] as $attachment) {
			if(!empty($attachment) && $attachment['type'] == 'attachment' ) {
				$candidate = [
					'date' => $email_body['date'],
					'job_description_id' => $jobDesc["_id"],
					'applicant_email' => $email_body['from']['value'][0]['address'],
					'applicant_name' => $email_body['from']['value'][0]['name']
				];
				$candidate['mail'] = $email_body;
				unset($candidate['mail']['attachments']);
				$_id = $resume->insertResume($candidate);
				if(empty($_id)){ return;}
				if($path = HelperController::getApplicationAttachmentPath($_id)) {
					$filePath = $path.DIRECTORY_SEPARATOR.$attachment['filename'];
					$fileData = '';
					foreach ($attachment['content']['data'] as $byte) {
						$fileData .= chr($byte);
					}
					file_put_contents($filePath, $fileData); 
					$candidate = [
						'file_name' => $attachment['filename'],
						'file_type' => $attachment['contentType'],
						'file' => $filePath,
						'data' => HelperController::matchResumeToPosting($filePath, $jobFields)	
					];
					$_id = $resume->updateResume($_id, $candidate);
				}
			}
		}
        return 'ok';
    }
}
