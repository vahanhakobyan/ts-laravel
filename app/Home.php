<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Home extends Model
{
    public static function getSkills()
    {
        $relations = DB::connection('mongodb')
            ->table('dictionary')
            ->where('type', 'skill')
            ->get(['name', 'relations']);

        $return = [];

        foreach ($relations as $item) {
            $return[] = $item['name'];
        }

        return $return;
    }

    public function getList($id = null, $skill_text = null, $str = null)
    {
        $project_id = $id;

        if(!empty($str)) {
            $str = trim($str);
        }

        $groups = new Group();

        $vars_projects = [];
        $vars_posts = [];
        $project_name = [];

        foreach ($groups->getAll() as $project) {
            $vars_projects[] = [
                'id' => $project->id,
                'name' => strlen($project->name) > 27 ? substr($project->name, 0, 25) . '..' : $project->name,
            ];
            $project_name[$project->id] = $project->name;
        }

        $vars_project_name = $project_name;

        $list = [];

        $description = new Description();

        foreach ($description->getAllApplications($project_id, $skill_text, $str) as $posts) {
            $skills = [];
            if (!empty($posts['fields']['skills'])) {
                foreach ($posts['fields']['skills'] as $skill) {
                    $d = 'years';
                    if($skill['experience'] == '< 1') {
                        $d = 'year';
                    }
                    else if($skill['experience'] == 'not required') {
                        $d = '';
                    }
                    if(mb_strlen($skill['skill']) > 13) {
                        $skill['skill'] = mb_substr($skill['skill'],0, 13) . '..';
                    }
                    $skills[] = '<i>' . $skill['skill'] . ' / ' . $skill['experience'] . ' '.$d.'</i>';
                }
            }
            $vars_posts[] = [
                '_id' => $posts['_id'],
                'project_id' => $posts['project_id'],
                'position' => $posts['fields']['position'],
                'seniority' => $posts['fields']['seniority'],
                'date' => date("M d, Y", time()), //$posts['date_created']->sec
                'email' => $posts['email'],
                'skills' => $skills
            ];
            $list[] = $posts['_id'];
        }

        $array = [];

        foreach ($description->getResultsForJobPosting($list) as $value) {
            $array[$value['job_description_id'] . ''][] = [
                'id' => $value['_id'],
                'data' => $value['data'],
                'file' => $value['file_type'] == 'application/msword' ? 'word' : 'pdf',
                'date' => $value['date']
            ];
        }

        $vars_resumes = $array;

        $vars = [
            'projects' => $vars_projects,
            'posts' => $vars_posts,
            'resumes' => $vars_resumes,
            'project_name' => $vars_project_name
        ];

        echo json_encode($vars);
    }

    public static function edit($name, $phone, $email)
    {
        DB::table('users')
            ->where('id', Auth::id())
            ->update([
                'name' => $name,
                'phone' => $phone,
                'email' => $email
            ]);

        return 'ok';
    }

    public function checkOldPassword($password)
    {
        $user = Auth::user();
        if (Hash::check($password, $user->getAuthPassword())) {
            return 'ok';
        }

        return 'wrong';
    }

    public function newPassword($password)
    {
        $user_id = Auth::id();
        DB::table('users')
            ->where('id', $user_id)
            ->update(['password' => Hash::make($password)]);

        return 'ok';
    }
}
