<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'user_id', 'name'
    ];

    public function getAll()
    {
        $user_id = Auth::id();

        return DB::table('groups')
            ->where(['user_id' => $user_id])
            ->orderByDesc('id')
            ->get();
    }
}
