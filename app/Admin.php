<?php

namespace App;

use App\Http\Controllers\HelperController;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function emailExists($email)
    {
        $user = DB::table('users')
            ->where('email', $email)
            ->first();

        if(is_null($user)) {
            return 'ok';
        }
        else {
            return 'exist';
        }
    }

    public static function addUser($name, $email)
    {
        $user_id = DB::table('users')
            ->insertGetId([
                'company_id' => 0,
                'role_id' => 1,
                'phone' => '',
                'password' => '',
                'name' => $name,
                'email' => $email,
                'status' => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

        $token = HelperController::generateCode(16);

        $res = DB::table('user_invite')
            ->insert([
            'user_id' => $user_id,
            'token' => $token
        ]);

        $to_name = $name;
        $to_email = $email;
        $data = [
            'token' => $token,
            'site' => 'http://textslice.eu/register'
        ];

        Mail::send('emails.send_invite', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)->subject('TextSlice Recruitment');
            $message->from('admin@textslice.eu', 'TextSlice Recruitment');
        });

        return $res;
    }

    public static function editUser($data)
    {
        DB::table('users')
            ->where('id', $data['id'])
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'status' => $data['status']
            ]);

        DB::table('company')
            ->where('id', $data['company_id'])
            ->update([
                'name' => $data['company_name'],
                'domain' => $data['company_domain']
            ]);

        return 'ok';
    }
}
