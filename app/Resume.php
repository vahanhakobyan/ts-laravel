<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Resume extends Model
{
	protected $elastic;
	
	function __construct(array $attributes = array()) 
	{
		parent::__construct($attributes);
		$this->elastic = app(Elastic\Elastic::class);
	}

	public function getAll()
    {
        return DB::connection('mongodb')
            ->table('job_applications')
            ->select(['date', 'applicant_email', 'applicant_name', 'file_name', 'file_type'])
            ->get();
    }
	public function insertResume($candidate, $pool_id=0) {
		if (empty($candidate)){return false;}
		$return = DB::connection('mongodb')
			->table('job_applications')
			->insertGetId(
			$candidate
		);
		$_id = (string) $return;

		if ($_id && !empty($candidate['text']) && $pool_id) {
			//Elasticsearhc
			$this->elastic->index([
				'id' => $_id,
				'body' => [
					'resume_id' => $_id,
					'pool_id' => $pool_id,
					'resume_content' => $candidate['text']
				]
			]);
		}
		return $_id;
	}

	public function updateResume($_id, $candidate, $pool_id=0) {
		if (empty($_id) || empty($candidate)) {return false;}
		$return = DB::connection('mongodb')
			->table('job_applications')
			->where('_id', $_id)
			->update(
				$candidate
			);
		if ($_id && !empty($candidate['text']) && $pool_id) {
			//Elasticsearhc
			$this->elastic->index([
				'id' => $_id,
				'body' => [
					'resume_id' => $_id,
					'pool_id' => $pool_id,
					'resume_content' => $candidate['text']
				]
			]);
		}
		return $_id;
	}

	public function getResume(Request $request)
	{
		if(empty($request['fields'])){
			$request['fields'] = [];
		}
		//todo
		return DB::connection('mongodb')
            ->table('job_applications')
			->where('_id',$request['id'])
            ->first($request['fields']);
	}
	
	public function searchResume($keyword)
	{
		$params = [
			'body' => [
				'query' => [			
					'bool' => [
						'must' => [
							[ 'match' => [ 
								'resume_content' => [
									'query' => $keyword,
									'fuzziness' => 'AUTO'
								]
								] 
							],
							[ 'match' => [ 'pool_id' => '1' ] ],
						]
					]
				]
			]
		];
		return $this->elastic->search($params);
	}
}