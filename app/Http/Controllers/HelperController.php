<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelperController extends Controller
{
    public static function generateCode($length = 8)
    {
        $chars = 'abdefhiknrstyz1234567890';
        $numChars = strlen($chars);
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }

        return $string;
    }

    public static function showUserStatus($status)
    {
//      0 - Pending - if he has not registered yet
//      1 - Active - service registered and paid
//      2 - Inactive - service registered and after test phase not paid
        switch ($status) {
            case 1:
                $status = 'Active';
                break;
            case 2:
                $status = 'Inactive';
                break;
            default:
                $status = 'Pending';
        }

        return $status;
    }

    public static function editUserStatus()
    {
         return ['Pending', 'Active', 'Inactive'];
    }
	
	public static function getApplicationAttachmentPath($_id)
    {
        $path = dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR;
		$path .= 'attachments'.DIRECTORY_SEPARATOR;
		$path .= date('Ymd').DIRECTORY_SEPARATOR;
		$path .= $_id;
		if( mkdir($path, 0777, true)) {
			return $path;
		}
    }
	
	public static function getApplicationAttachmentText($file_path)
    {
        $data = ["data" => base64_encode(file_get_contents($file_path))];
		if(empty($data))return '';
		
		$data = http_build_query($data);
		if( $curl = curl_init() ) {
			curl_setopt($curl, CURLOPT_URL, 'http://18.215.26.244:5500/atom/convert');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);    
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$out = json_decode(curl_exec($curl));
			curl_close($curl);      
		}
		return $out;
    }
	
	public static function matchResumeToPosting($resume_file_path, $posting_fields)
    {
        $input = [
			'file' => base64_encode(file_get_contents($resume_file_path)),
			'posting' => $posting_fields
		];
		$data = ["input" => json_encode($input)];
		$data = http_build_query($data);
		if( $curl = curl_init() ) {
			curl_setopt($curl, CURLOPT_URL, 'http://18.215.26.244:5500/flow/compare');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);    
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$out = curl_exec($curl);
			curl_close($curl);      
		}
		if(!empty($out)){return json_decode($out, true);}
		return ;
    }
	
}
