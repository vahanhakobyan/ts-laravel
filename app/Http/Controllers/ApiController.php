<?php

namespace App\Http\Controllers;

use App\Api;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function get_resumes()
    {
        return Api::getAll();
    }
    public function new_mail(Request $request)
    {
		$api = new Api();
        return $api->saveNewMail($request->input());
    }
}
