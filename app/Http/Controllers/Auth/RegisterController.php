<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request)
    {
        $key = DB::table('settings')->where('key', 'user_registration_type')->first(['value']);

        $user = [];

        if ($key->value) {
            $user = DB::table('user_invite')
                ->where('token', $request['token'])
                ->leftJoin('users', 'users.id', '=', 'user_invite.user_id')
                ->first(['name', 'email']);

            if(!$user) {
                return redirect('/');
            }
        }

        return view('auth.register', ['user' => $user]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $company_id = DB::table('company')
            ->insertGetId([
                'name' => $data['company'],
                'domain' => $data['domain'],
                'invite_email' => $data['email'],
            ]);

        $setting = DB::table('settings')
            ->where('key', 'user_registration_type')
            ->first(['value']);
        dd($setting->value);
        if($setting->value == 1) {
            $user = new User();

            $user_id = $user->editUser($data, $company_id);



        }
        else {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => '',
                'company_id' => $company_id,
                'role_id' => 1,
                'password' => Hash::make($data['password']),
            ]);

            $user_id = $user->id;
        }

        DB::table('company')
            ->where('id', $company_id)
            ->update([
                'creator_id' => $user_id
            ]);

        return $user;
    }
}
