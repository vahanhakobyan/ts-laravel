<?php

namespace App\Http\Controllers;

use App\Resume;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public function index()
    {
        $resumes = new Resume();

        return view('admin.resumes', ['resumes' => $resumes->getAll()]);
    }

	public function search($keyword)
    {
        $resumes = new Resume();
		$result = $resumes->searchResume($keyword);
		echo '<pre>';print_r($result);echo'</pre>';
	}
}
