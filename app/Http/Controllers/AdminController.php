<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Resume;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admin');
    }

    /**
     * Show the TS Admins.
     *
     * @return \Illuminate\Http\Response
     */
    public function admins()
    {
        $admins = Admin::all();

        return view('admin.admins', ['admins' => $admins]);
    }

    public function users(Request $request)
    {
        if($request['edit_user']) {
            Admin::editUser($request->input());
        }
        else if ($request['name'] && $request['email']) {
            Admin::addUser($request['name'], $request['email']);
        }

        $users = new User();

        return view('admin.users', [
            'users' => $users->getAll(),
            'settings' => DB::table('settings')->where('key', 'user_registration_type')->first(['value'])
        ]);
    }

    public function settings()
    {
        $settings = DB::table('settings')->pluck('value', 'key');

        return view('admin.users_settings', ['settings' => $settings]);
    }

    public function post(Request $request)
    {
        switch ($request['action']) {
            case 'update':
                DB::table('settings')
                    ->where('key', 'user_registration_type')
                    ->update(['value' => $request['user_registration_type']]);
                break;
            case 'email_exists':
                if(Admin::emailExists($request['email']) === 'ok') {
                    return 'ok';
                }
                else {
                    return back()->with('error', '');
                }
                break;
        }

        return 'ok';
    }

    /**
     * Show the TS Roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        $roles = Role::all();

        return view('admin.roles', ['roles' => $roles]);
    }

    /**
     * Show the Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function role($id = null)
    {
        $role = [];

        if ($id) {
            $role = Role::query()->where('id', $id);
        }

        return view('admin.role', ['role' => $role]);
    }

    public function user_edit($id)
    {
        $user = new User();

        return view('admin.user_edit', ['user' => $user->getUserById($id)]);
    }

    public function user_delete(Request $request)
    {
        DB::table('users')
            ->delete(['id' => $request['user_id']]);

        DB::table('company')
            ->delete(['creator_id' => $request['user_id']]);

        DB::table('user_invite')
            ->delete(['user_id' => $request['user_id']]);

        return redirect()->route('admin.users');
    }
}
