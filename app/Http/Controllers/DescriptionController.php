<?php

namespace App\Http\Controllers;

use App\Description;
use Illuminate\Http\Request;

class DescriptionController extends Controller
{
    public function index()
    {
        $descriptions = new Description();

        return view('admin.descriptions', ['descriptions' => $descriptions->getAll()]);
    }

    public function add(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->setDescription(
            $request['seniority'],
            $request['job_posting_name'],
            $request['group_name'],
            $request['group_id']
        );
    }

    public function edit(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->editDescription($request['id']);
    }

    public function get(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->getByGroup($request['id']);
    }

    public function update_jp(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateJP(
            $request['id'],
            $request['st']
        );
    }

    public function update_hi(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateHI(
            $request['id'],
            $request['posting'],
            $request['level'],
            $request['total_experience']
        );
    }

    public function update_job_posting_skill(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateJobPostingSkill(
            $request['id'],
            $request['jp1'],
            $request['sk']
        );
    }

    public function update_job_posting_education(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateJobPostingEducation(
            $request['id'],
            $request['edu']
        );
    }

    public function update_job_posting_qr(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateJobPostingQR(
            $request['id'],
            $request['jp5'],
            $request['jp6']
        );
    }

    public function summary_report(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->summaryReport(
            $request['jp'],
            $request['arr']
        );
    }

    public function remove_job_posting(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->removeJobPosting($request['id']);
    }

    public function update_job_posting(Request $request)
    {
        $descriptions = new Description();

        return $descriptions->updateJobPosting($request['id'], $request['txt']);
    }

    public function load_select_boxes()
    {
        $descriptions = new Description();

        return $descriptions->loadSelectBoxes();
    }
}
