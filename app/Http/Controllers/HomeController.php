<?php

namespace App\Http\Controllers;

use App\Description;
use App\Group;
use App\Home;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = new Group();
        $description = new Description();

        $vars_projects = [];
        $vars_posts = [];
        $project_name = [];

        if(!empty($groups->getAll())) {
            foreach ($groups->getAll() as $key => $project) {
                $vars_projects[] = [
                    'id' => $project->id,
                    'name' => $project->name,
                ];
                $project_name[$project->id] = $project->name;
            }
        }

        $vars_project_name = $project_name;

        $list = [];

        $array_app = $description->getAllApplications();

        foreach ($array_app as $key => $posts) {
            $vars_posts[] = [
                'posts' => $posts
            ];
            $list[] = $posts['_id'].'';
        }

        $result = [];

        foreach ($array_app as $fields) {
            if(!empty($fields['fields']['skills'])) {
                foreach ($fields['fields']['skills'] as $skill) {
                    if (!empty($skill['skill'])) {
                        $result[] = $skill['skill'];
                    }
                }
            }
        }

        $vars_skills = array_unique($result);

        $array = [];

        foreach ($description->getResultsForJobPosting($list) as $value) {
            if(!empty($value['data'])) {
                $array[$value['job_description_id'] . ''][] = [
                    'id' => $value['_id'],
                    'data' => $value['data'],
                    'file' => $value['file_type'] == 'application/msword' ? 'word' : 'pdf',
                    'date' => $value['date']
                ];
            }
        }

        $vars_resumes = $array;

        $vars_posted_this_week_jobs = $description->getAllJobsCount();
        $vars_posted_this_week_applicants = $description->getAllApplicationsCount();

        if(count($vars_posts)==0) {
            $vars_company = $description->getCompanyByUserId();
        }
        else {
            $vars_company = '';
        }

        return view('home', [
            'groups' => $groups->getAll(),
            'skills' => $vars_skills,
            'posts' => $vars_posts,
            'project_name' => $vars_project_name,
            'projects' => $vars_projects,
            'resumes' => $vars_resumes,
            'posted_this_week_jobs' => $vars_posted_this_week_jobs,
            'posted_this_week_applicants' => $vars_posted_this_week_applicants,
            'company' => $vars_company
        ]);
    }

    public function welcome()
    {
        if($user = Auth::user())
        {
            return redirect('/home');
        }

        return view('welcome', ['settings' => DB::table('settings')->where('key', 'user_registration_type')->first(['value'])]);
    }

    public function all_skills()
    {
        $skills = new Home();

        return $skills->getSkills();
    }

    public function get_list(Request $request)
    {
        $items = new Home();

        return $items->getList($request['id'], $request['skill_text'], $request['str']);
    }

    public function profile()
    {
        $groups = new Group();
        $user = Auth::user();

        return view('auth.profile', [
            'groups' => $groups->getAll(),
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        Home::edit($request['name'], $request['phone'], $request['email']);

        return 'ok';
    }

    public function old_password(Request $request)
    {
        $password = new Home();

        return $password->checkOldPassword($request['password']);
    }

    public function new_password(Request $request)
    {
        $password = new Home();

        return $password->newPassword($request['np']);
    }
}
