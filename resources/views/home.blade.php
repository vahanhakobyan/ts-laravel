@extends('layouts.home')

@section('content')
<div class="left-panel">
    <a id="logo" href="/"><img src="/assets/css/lib/heyling/logo.png" alt=""></a>
    <a id="left-open-dashboard"><img src="/assets/css/lib/heyling/dashboard.png" alt=""></a>
    <a id="left-create-team" title="Create Group"></a>
    <a id="left-create-job" title="Create Job Posting"></a>
    <a id="user-logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"></a>
    <a id="user-edit" href="{{ url('profile') }}" title="Edit Profile"></a>
    <a id="left-user"></a>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<div id="left-side">
    <div class="textslice-menu">
        <a id="slide-menu"><img src="/assets/css/lib/heyling/menu.png" alt=""></a>
    </div>
    <ul>
        <li><a href="/" id="left-dashboard">Dashboard</a></li>
        <li>
            <div id="left-teams">Groups</div>
        </li>
        <li class="separate"><span></span></li>
    </ul>
    <a class="groups_add_btn">Groups</a>
    <ul class="with-scroll">
        @if(count($groups) > 0)
            @foreach($groups as $group)
                <li class="team_name" id="{{ $group->id }}">{{ $group->name }}</li>
            @endforeach
        @else
            <li class="no-have">Create your first <a class="link-group-create">group</a>.</li>
        @endif
    </ul>
</div>

<div id="main-content">
    <div class="main-title">
        <h2>Dashboard</h2>
        <h6>Check out what’s hapenning!</h6>
    </div>
    <div class="row">
        <div class="col welcome-box">
            <div>
                <h3>Welcome to TextSlice Recruitment</h3>
                Create a new <a class="add-new-group">Group</a> or a new <a class="add-new-job-posting">Job Posting</a>
                to start. You always can edit or remove the groups and postings before you make them public and receive
                applications.<br>
                The dashboard automatically reflects the latest candidate activities, up to date reports and insights.
            </div>
        </div>
        <div class="col jobs-box">
            <h3><b><?php echo $posted_this_week_jobs, $posted_this_week_jobs==1 ? ' Posting' : ' Postings'; ?></b></h3>
            <span>created this week</span>
        </div>
        <div class="col applicants-box">
            <h3><b><?php echo $posted_this_week_applicants, $posted_this_week_applicants==1 ? ' Applicant' : ' Applicants'; ?></b></h3>
            <span>applied this week</span>
        </div>
    </div>

    <div class="dotted-separate"></div>
    @if(count($posts) > 0)
        <div class="main">
            <div class="main-filters">
                <ul>
                    <li class="filter-projects">Groups:</li>
                    <li class="filter-projects-input">
                        <div>All</div>
                        <p>
                            @foreach($groups as $group)
                                <a data-id="{{ $group->id }}">{{ $group->name }}</a>
                            @endforeach
                        </p>
                    </li>
                    <li class="filter-skills">Skills:</li>
                    <li class="filter-skills-input">
                        <div>All</div>
                        @if(count($skills) > 0)
                            <p>
                                @foreach($skills as $skill)
                                    <a>{{ $skill }}</a>
                                @endforeach
                            </p>
                        @endif
                    </li>
                    <li class="filter-search"><input type="text" placeholder="Search"><a><img src="/assets/css/lib/heyling/search.png" alt=""></a></li>
                </ul>
            </div>
            <div class="main-list">
                @php
                    $i = 0;
                @endphp
                @foreach($posts as $post)
                    <div
                        @if($i%2)
                            class="ml"
                        @endif
                    >
                        <div class="top-section">
                            <strong>
                                <a data-id="{{ $post['posts']['_id'].'' }}">
                                @if(strlen($post['posts']['fields']['position']) > 50)
                                    {{ str_limit($post['posts']['fields']['position'], $limit = 50, $end = '..') }}
                                @else
                                    {{ trim($post['posts']['fields']['position']) ? $post['posts']['fields']['position'] : '-' }}
                                @endif
                                </a>
                            </strong>
                            <date>{{ date("M d, Y", strtotime($post['posts']['date_created']['date'])) }}</date>
                            <span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>
                            <span>
                                Resumes:
                                @if(isset($resumes[$post['posts']['_id'].'']))
                                    @if(count($resumes[$post['posts']['_id'].''])>0)
                                        <a data-jd="{{ $post['posts']['_id'] }}" data-pi="{{ $post['posts']['project_id'] }}">{{ count($resumes[$post['posts']['_id'] . '']) }}</a>
                                    @else
                                        0
                                    @endif
                                @else
                                    0
                                @endif
                            </span>
                            <div>
                                Group: <b>
                                    @if(strlen($project_name[$post['posts']['project_id'].'']) > 25)
                                        {{ str_limit($project_name[$post['posts']['project_id'].''], $limit = 23, $end = '..') }}
                                    @else
                                        {{ $project_name[$post['posts']['project_id'].''] }}
                                    @endif
                                </b>
                                <mail>{{ $post['posts']['email'] }}</mail>
                            </div>
                        </div>
                        <div class="bottom-section">
                            <dd>Skills:</dd>
                            @if(isset($post['posts']['fields']['skills']) && count($post['posts']['fields']['skills'])>0)
                                @php
                                    $c = $n = 0;
                                    $skills_array = [];
                                    $str_count = 0;
                                @endphp
                                @foreach($post['posts']['fields']['skills'] as $skills)
                                    @php
                                        $d = 'years';
                                    @endphp
                                    @if($skills['experience'] == '< 1')
                                        @php
                                            $d = 'year';
                                        @endphp
                                    @elseif($skills['experience'] == 'not required')
                                        @php
                                            $d = '';
                                        @endphp
                                    @endif
                                    @if($c < 3)
                                        @php
                                            $str_count += mb_strlen($skills['skill']);
                                        @endphp
                                        @if(mb_strlen($skills['skill']) > 13)
                                            @php
                                                $skills['skill'] = mb_substr($skills['skill'], 0, 13) . '..';
                                            @endphp
                                        @endif
                                        @php
                                            $skills_array[] = '<i>'.$skills['skill'].' / '.$skills['experience'].' '.$d.'</i>';
                                            $n++;
                                        @endphp
                                    @endif
                                    @php
                                        $c++;
                                    @endphp
                                @endforeach
                                @if($str_count > 50 && ($c - $n) > 0)
                                    @php
                                        $skills_array[] = '<em>+' . ($c - ($n-1)) . '</em>';
                                        array_shift($skills_array);
                                    @endphp
                                @elseif(($c - $n) > 0)
                                    @php
                                        $skills_array[] = '<em>+' . ($c - $n) . '</em>';
                                    @endphp
                                @endif
                                @foreach(array_reverse($skills_array) as $sa)
                                    {!! $sa !!}
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @php
                    $i++;
                    @endphp
                @endforeach
            </div>
        </div>
    @else
        <p class="empty-main hide-first-post">
            <img src="/assets/css/lib/heyling/empty.png" alt="">
        </p>
        <h4 class="hide-first-post">
            Welcome to
            @if(!empty($company->name))
                {{ $company->name }}
            @endif
            dashboard.</h4>
        <br>
        <p class="center hide-first-post">
            Start by creating your first <a class="link-group-create">group</a> or <a class="cr-new-job-posting">job posting</a>.
            You always can edit or remove the groups and postings before you start receiving candidate applications for your open jobs.
        </p>
        <p class="hide-first-post">&nbsp;</p>
        <p class="center hide-first-post">
            <a class="btn btn-create-team">Create Group</a> <span class="or">or</span> <a class="btn btn-create-job-posting">Create Job Posting</a>
        </p>
    @endif
</div>
@endsection
