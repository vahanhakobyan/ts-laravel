<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="TS Admin - Admin Panel">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
</head>
<body>

@include('layouts.admin-left')

@yield('admin-content')

<script src="{{ asset('assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
{{--<script src="{{ asset('assets/js/lib/chart-js/Chart.bundle.js') }}"></script>--}}
<script src="{{ asset('assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/widgets.js') }}"></script>
<script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.js') }}"></script>
<script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/vector-map/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ asset('assets/js/lib/vector-map/country/jquery.vmap.world.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('/assets/js/lib/data-table/datatables-init.js') }}"></script>
<script src="{{ asset('/assets/js/admin.js') }}"></script>
</body>
</html>
