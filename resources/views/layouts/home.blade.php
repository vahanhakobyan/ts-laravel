<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TextSlice</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900" rel="stylesheet">
    <link href="{{ asset('assets/css/heyling.css') }}" rel="stylesheet">
</head>
<body>
@yield('content')
<input type="text" id="hidden_input" name="hidden_input" style="position:fixed;top:-9999px;">
<script src="{{ asset('assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('assets/js/tether.js') }}"></script>
<script src="{{ asset('assets/js/heyling.js') }}"></script>
<script src="{{ asset('assets/js/jquery.canvasjs.min.js') }}"></script>
</body>
</html>
