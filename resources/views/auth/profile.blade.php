@extends('layouts.home')

@section('content')
    <div class="left-panel">
        <a id="logo" href="/"><img src="/assets/css/lib/heyling/logo.png" alt=""></a>
        <a id="left-open-dashboard"><img src="/assets/css/lib/heyling/dashboard.png" alt=""></a>
        <a id="left-create-team" title="Create Group"></a>
        <a id="left-create-job" title="Create Job Posting"></a>
        <a id="user-logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"></a>
        <a id="user-edit" href="{{ url('profile') }}" title="Edit Profile"></a>
        <a id="left-user"></a>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <div id="left-side">
        <div class="textslice-menu">
            <a id="slide-menu"><img src="/assets/css/lib/heyling/menu.png" alt=""></a>
        </div>
        <ul>
            <li><a href="/" id="left-dashboard">Dashboard</a></li>
            <li>
                <div id="left-teams">Groups</div>
            </li>
            <li class="separate"><span></span></li>
        </ul>
        <a class="groups_add_btn">Groups</a>
        <ul class="with-scroll">
            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <li class="team_name" id="{{ $group->id }}">{{ $group->name }}</li>
                @endforeach
            @else
                <li class="no-have">Create your first <a class="link-group-create">group</a>.</li>
            @endif
        </ul>
    </div>

    <div id="main-content">
        <div class="main-title">
            <h2>Edit Profile</h2>
            <br>
            <span> {{ $user->name }} </span>
        </div>

        <div class="personal_page">
            <h2><span>User Information</span></h2>
            <div class="form-field">
                <label for="name" style="padding-top: 30px;">Name</label>
                <div class="select-education">
                    <input class="select" id="name" value="{{ $user->name }}">
                </div>
            </div><div class="form-field">
                <label for="phone" style="padding-top: 30px;">Phone</label>
                <div class="select-education">
                    <input class="select" id="phone" value="{{ $user->phone }}">
                </div>
            </div><div class="form-field">
                <label for="email" style="padding-top: 30px;">Email</label>
                <div class="select-education">
                    <input class="select" id="email" value="{{ $user->email }}">
                </div>
            </div>
            <div class="form-field">
                <div class="select-education">
                    <input type="button" class="select" id="update-profile" value="Update">
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="personal_page">
            <h2><span>Change Password</span></h2>

            <div class="form-field">
                <div class="select-education">
                    <input type="button" class="select" id="change-password" value="Change Password">
                </div>
            </div>

            <div class="form-field change_password">
                <label for="current-password" style="padding-top: 30px;">Current Password</label>
                <div class="select-education">
                    <input type="password" class="select" id="current-password">
                </div>
            </div>
            <div class="form-field change_password">
                <label for="new-password">New Password</label>
                <div class="select-education">
                    <input type="password" class="select" id="new-password">
                </div>
            </div>
            <div class="form-field change_password">
                <label for="re-type-new-password">Re-type New Password</label>
                <div class="select-education">
                    <input type="password" class="select" id="re-type-new-password">
                </div>
            </div>
            <div class="form-field change_password">
                <div class="select-education">
                    <input type="button" class="select" id="update-password" value="Update Password">
                </div>
            </div>
        </div>

    </div>

@endsection
