<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TextSlice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/s_2.css') }}">
</head>
<body>

    <div class="site-wrapper">
        <span class="pattern"></span>
        <div class="site-wrapper-inner">
            <div class="content">
                <img src="/images/chip-left-top.png" alt="">
                <img src="/images/chip-left-bottom.png" alt="">
                <img src="/images/chip-rigth-bottom.png" alt="">
                <div class="container-fluid height-100">
                    <div class="row height-100">
                        <div class="col-md-6 clear-pad hidden-sm-down">
                            <div class="textslice height-100">
                                <div class="sidebar">
                                    <a href="">
                                        <img src="/images/sign-up.png" alt="">
                                        <p>Sign Up</p>
                                    </a>
                                </div>
                                <div class="textslice-content">
                                    <img src="/images/robot.png" alt="">
                                    <a class="navbar-brand" href="/"><span class="logo"> </span></a>
                                    <div class="text-content">
                                        <h4>Welcome to Textslice.</h4>
                                        <p>
                                            Your AI Powered Recruitment Assistant.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 clear-pad tc-txt">
                            <div class="textslice-content2">
                                <img src="/images/robot.png" alt="">
                                <a class="navbar-brand" href="/"><span class="logo2"> </span></a>
                            </div>
                            <div class="signUp height-100">
                                <h6><i class="fa fa-circle align-middle" aria-hidden="true"></i> SIGN UP <i class="fa fa-circle align-middle" aria-hidden="true"></i></h6>
                                <div class="row">
                                    <div class="col-sm-12 error-msg-2"></div>
                                </div>
                                <form id="signUp" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input placeholder="Name"
                                               id="input-name"
                                               type="text"
                                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name"
                                               value="@if($user) {{ $user->name }} @else {{ old('name') }} @endif"
                                               required>
                                        <img src="/images/name.png" alt="" class="name-icon">
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Email"
                                               id="input-email"
                                               type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email"
                                               value="@if($user) {{ $user->email }} @else {{ old('email') }} @endif"
                                               required>
                                        <img src="/images/email.png" alt="" class="name-icon">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Password" id="input-password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        <img src="/images/pass.png" alt="" class="name-icon">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Password Confirmation" id="confirm-password" type="password" class="form-control" name="password_confirmation" required>
                                        <img src="/images/pass.png" alt="" class="name-icon">
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Domain" type="text" class="form-control" name="domain" id="input-domain" value="{{ old('domain') }}" required>
                                        <img src="/images/domain.png" alt="" class="name-icon">
                                        <div class="domain"></div>
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Company" type="text" class="form-control" name="company" value="{{ old('company') }}" id="company-name" required>
                                        <img src="/images/company_icon.png" alt="" class="name-icon">
                                    </div>
                                    <div class="form-check">
                                        <div class="form-check-label">
                                            <input type="checkbox" id="i_agree" class="form-check-input">
                                            <label for="i_agree">I agree to</label> <a class="terms-condition">Terms and Conditions</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btnLogIn" id="btnGetStarted">
                                        {{ __('Get Started') }}
                                    </button>
                                    <p> Already a member? <a href="/" style="color:#da394b;">Sign in</a> </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('assets/js/tether.js') }}"></script>
    <script src="{{ asset('assets/js/ie10-viewport-bug-workaround.js') }}"></script>
</body>
</html>
