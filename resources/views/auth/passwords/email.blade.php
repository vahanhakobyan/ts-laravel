@extends('layouts.login')

@section('content')
    <div class="site-wrapper">
        <span class="pattern"></span>
        <div class="site-wrapper-inner">
            <div class="content">
                <img src="/images/chip-left-top.png" alt="">
                <img src="/images/chip-left-bottom.png" alt="">
                <img src="/images/chip-right-bottom.png" alt="">
                <div class="container-fluid height-100">
                    <div class="row height-100">
                        <div class="col-md-6 clear-pad hidden-sm-down">
                            <div class="textslice height-100">
                                <div class="sidebar">
                                    <a href="/">
                                        <img src="/images/sign-in.png" alt="">
                                        <p>Sign In</p>
                                    </a>
                                </div>
                                <div class="textslice-content">
                                    <img src="/images/robot.png" alt="">
                                    <a class="navbar-brand" href="/"><span class="logo"> </span></a>
                                    <div class="text-content">
                                        <h4>Welcome to Textslice.</h4>
                                        <p>
                                            Your AI Powered Recruitment Assistant.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 clear-pad">
                            <div class="textslice-content2">
                                <img src="/images/robot.png" alt="">
                                <a class="navbar-brand" href="/"><span class="logo2"> </span></a>
                            </div>
                            <div class="signIn height-100">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="confirmation">
                                        <h6><i class="fa fa-circle align-middle" aria-hidden="true">
                                            </i> FORGOT PASSWORD <i class="fa fa-circle align-middle" aria-hidden="true"></i>
                                        </h6>
                                        <div class="row">
                                            <div class="col-sm-12 error-msg-3"></div>
                                        </div>
                                        <span>
                                            @if (session('status'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                                        <p>
                                            Please provide the email address that you used when you signed up for your Textslice account.
                                            We will send you an email that will allow you to reset your password.
                                        </p>
                                    </span>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="email address" value="{{ old('email') }}" required>
                                        <img src="/images/emails_login.png" alt="" class="name-icon email-icon">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <button type="submit" class="btnGetStarted">
                                        {{ __('Submit') }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
