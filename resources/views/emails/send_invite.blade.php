<strong>Hey there!</strong>,

<p>
    We are happy to invite you to use the TextSlice Recruitment application. <br>
    As you know, we are in a 'closed beta' phase and we highly appreciate you becoming part of it. <br>
    We look forward to your valuable input and we thank you for sharing these exciting times with our team. <br>
    Click to <a href="{{ $site }}/{{ $token }}">start</a>

    Sincerely,
    TextSlice team.
</p>
