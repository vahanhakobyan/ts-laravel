@extends('layouts.login')

@section('content')
<div class="site-wrapper">
    <span class="pattern"></span>
    <div class="site-wrapper-inner">
        <div class="content">
            <img src="/images/chip-left-top.png" alt="">
            <img src="/images/chip-left-bottom.png" alt="">
            <img src="/images/chip-right-bottom.png" alt="">
            <div class="container-fluid height-100">
                <div class="row height-100">
                    <div class="col-md-6 clear-pad hidden-sm-down">
                        <div class="textslice height-100">
                            <div class="sidebar">
                                <a href="/">
                                    <img src="/images/sign-in.png" alt="">
                                    <p>Sign In</p>
                                </a>
                            </div>
                            <div class="textslice-content">
                                <img src="/images/robot.png" alt="">
                                <a class="navbar-brand" href="/"><span class="logo"> </span></a>
                                <div class="text-content">
                                    <h4>Welcome to Textslice.</h4>
                                    <p>
                                        Your AI Powered Recruitment Assistant.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 clear-pad">
                        <div class="textslice-content2">
                            <img src="/images/robot.png" alt="">
                            <a class="navbar-brand" href="/"><span class="logo2"> </span></a>
                        </div>
                        <div class="signIn height-100">
                            <h6> <i class="fa fa-circle align-middle" aria-hidden="true"></i> SIGN IN <i class="fa fa-circle align-middle" aria-hidden="true"></i> </h6>
                            <div class="row">
                                <div class="col-sm-12 error-msg"></div>
                            </div>
                            <form method="POST" action="{{ route('login') }}" id="signIn">
                                @csrf
                                <div class="form-group ">
                                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="email address" name="email" id="email" value="{{ old('email') }}">
                                    <img src="/images/name.png" alt="" class="name-icon">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group ">
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="password" name="password" id="password">
                                    <img src="/images/pass.png" alt="" class="name-icon">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Remember me
                                    </label>
                                    <a href="{{ route('password.request') }}">
                                        {{ __('Forgot Password') }}
                                    </a>
                                </div>
                                <button type="submit" class="btnLogIn">{{ __('Log In') }}</button>
                                @if($settings->value == 0)
                                <p>Are you not registered yet? <a href="/register" style="color:#da394b;">Sign up now!</a></p>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection
