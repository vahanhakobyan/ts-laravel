$(document).ready(function () {
    var token = $('meta[name="csrf-token"]').attr('content');

    var options = {
        leftPanel: false,
        backPath: false,
        backPathId: false,
        mobileIs: false
    };

    function mobileVersion() {
        if($(window).width() <= 768 || $(window).height() <= 768) {
            $('.left-panel').append('<a id="open-slide"></a>');
            var mw = $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').width();
            $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').css('width', (+mw + 235) + 'px');
            options.mobileIs = true;
        }
    }

    mobileVersion();

    function rtrim(stringToTrim) {
        return stringToTrim.replace(/,\s*$/, "");
    }

    function ParseData(date) {
        var dt = new Date(date * 1000);
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        return (day < 10 ? '0' + day : day) + "." + (month < 10 ? '0' + month : month) + "." + year;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function sizeCorrections() {
        var winHeight = $(window).height();
        var winWidth = $(window).width();
        $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').css('width', (winWidth - 871) + 'px');
        $('.with-scroll').css('height', (winHeight - 312) + 'px');
        $('.for-white-link').css('height', (winHeight - 385) + 'px');
    }

    window.onresize = function () {
        sizeCorrections();
        mobileVersion();
    };

    sizeCorrections();

    $(document).on('click', 'body', function (e) {
        if (e.target.offsetParent) {
            if (typeof(e.target.offsetParent.classList[0]) != "undefined" &&
                e.target.offsetParent.classList[0] !== null &&
                e.target.offsetParent.classList[0] === 'filter-skills-input') {

            }
            else if (typeof(e.target.offsetParent.classList[0]) != "undefined" &&
                e.target.offsetParent.classList[0] !== null &&
                e.target.offsetParent.classList[0] === 'filter-projects-input') {

            }
            else if (typeof(e.target.offsetParent.classList[0]) != "undefined" &&
                e.target.offsetParent.classList[0] !== null &&
                e.target.offsetParent.classList[0] === 'filter-summary-skills-input') {

            }
            else if (typeof(e.target.offsetParent.classList[0]) != "undefined" &&
                e.target.offsetParent.classList[0] !== null &&
                e.target.offsetParent.classList[0] === 'filter-ps-input') {

            }
            else if (typeof(e.target.offsetParent.classList[0]) != "undefined" &&
                e.target.offsetParent.classList[0] !== null &&
                e.target.offsetParent.classList[0] === 'filter-hidden-input') {

            }
            else {
                $('.filter-skills-input > p').hide();
                $('.filter-projects-input > p').hide();
                $('.filter-ps-input > p').hide();
                $('.filter-summary-skills-input > p').hide();
                $('.filter-hidden-input > p').hide();
            }
        }
        else {
            $('.filter-skills-input > p').hide();
            $('.filter-projects-input > p').hide();
            $('.filter-ps-input > p').hide();
            $('.filter-summary-skills-input > p').hide();
            $('.filter-hidden-input > p').hide();
        }
        if (e.target.className !== 'jp_header_sl') {
            $('li.jp_header_sl dd').remove();
        }
    });

    $(document).on('click', '#slide-menu', function () {
        $('#left-side').css('left', '-235px');
        if(options.mobileIs) {
            $('.main-title').css('padding-left', '0');
        }
        else {
            $('.main-title').css('padding-left', '50px');
        }
        $('#main-content, .opened-jd').css('left', '68px');
        $('#left-open-dashboard').css('position', 'static');
        $('.left-panel').append('<a id="open-slide"></a>');
        var mw = $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').width();
        $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').css('width', (+mw + 235) + 'px');
        options.leftPanel = true;
    });

    $(document).on('click', '#open-slide', function () {
        $('#left-side').css('left', '68px');
        $('.main-title').css('padding-left', '0');
        $('#open-slide').remove();
        if(options.mobileIs) {
            $('#main-content, .opened-jd').css('left', '68px');
        }
        else {
            $('#main-content, .opened-jd').css('left', '303px');
        }
        $('#left-open-dashboard').css('position', 'absolute');
        var mw = $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').width();
        $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').css('width', (mw - 235) + 'px');
        options.leftPanel = false;
    });

    $(document).on('click', '#left-user', function () {
        var $this = $(this);
        if ($this.hasClass('open')) {
            $this.removeClass('open');
            $('#user-logout, #user-edit').hide();
        }
        else {
            $this.addClass('open');
            $('#user-logout, #user-edit').show();
        }
    });

    $(document).on('click', '#change-password', function () {
        var $this = $(this);
        if ($this.val() === 'Change Password') {
            $this.val('Cancel');
            $('.change_password').show();
        }
        else {
            $this.val('Change Password');
            $('.change_password').hide();
        }
    });

    $(document).on('click', '#update-profile', function () {
        var name = $('#name'),
            phone = $('#phone'),
            email = $('#email');
        name.attr('placeholder', '');
        phone.attr('placeholder', '');
        email.attr('placeholder', '');
        if (name.val() === '') {
            name.attr('placeholder', 'Please enter your name!');
            name.focus();
        }
        else if (phone.val() === '') {
            phone.attr('placeholder', 'Please enter your phone number!');
            phone.focus();
        }
        else if (email.val() === '') {
            email.attr('placeholder', 'Please enter your email!');
            email.focus();
        }
        else if (!validateEmail(email.val())) {
            email.attr('placeholder', 'Please enter valid email!');
            email.focus();
        }
        else {
            $.ajax({
                url: '/profile/update',
                //dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    $('#user-create').html('<i class="fa fa-cog fa-spin fa-3x fa-fw"></i> <span class="sr-only">Loading...</span>');
                },
                contentType: 'application/json',
                data: JSON.stringify({name: name.val(), phone: phone.val(), email: email.val(), _token: token}),
                processData: false,
                success: function (data) {
                    if (data.status == 'ok') {
                        showMsg('Changes Have Been Saves Successfully!', 'success', 5000);
                    }
                    else if (data.status == 'err') {
                        alert('Error!');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    });

    $(document).on('click', '#update-password', function () {
        var old = $('#current-password');
        var nps = $('#new-password');
        var cps = $('#re-type-new-password');
        $('.change_password input').attr('placeholder', '');
        if (old.val() == '') {
            old.closest('.change_password').find('input').attr('placeholder', 'Enter Old Password');
        }
        else if (old.val() != '') {
            $.ajax({
                url: '/profile/old_password',
                type: 'post',
                data: {password: old.val(), _token: token},
                success: function (d) {
                    if (d === 'ok') {
                        if (nps.val() == '') {
                            nps.closest('.change_password').find('input').val('').attr('placeholder', 'Enter new password').focus();
                        }
                        else if (cps.val() == '') {
                            cps.closest('.change_password').find('input').val('').attr('placeholder', 'Repeat new password').focus();
                        }
                        else if (nps.val().localeCompare(cps.val()) != 0) {
                            cps.closest('.change_password').find('input').val('').attr('placeholder', 'Entered passwords do not match').focus();
                        }
                        else {
                            $.ajax({
                                url: '/profile/new_password',
                                type: 'post',
                                data: {np: cps.val(), _token: token},
                                success: function (d) {
                                    if (d == 'ok') {
                                        $('.change_password').hide();
                                        $('#change-password').val('Change Password');
                                        showMsg('Changes Have Been Saves Successfully!', 'success', 5000);
                                    }
                                }
                            });
                        }
                    }
                    else {
                        old.closest('.change_password').find('input').val('').attr('placeholder', 'Old password is wrong');
                    }
                }
            });
        }
        return false;
    });

    function showMsg(txt, type, period) {
        $('body').append('<div class="' + type + '">' + txt + '<img src="/assets/css/lib/heyling/msg-x.png" alt=""></div>');
        setTimeout(function () {
            $('.' + type).css('top', '200px');
        }, (period / 10));
        setTimeout(function () {
            $('.' + type).remove();
        }, period);
    }

    $(document).on('click', '.success img, .error img, .warning img', function () {
        $(this).closest('div').remove();
    });

    $(document).on('click', '#left-create-job, .btn.btn-create-job-posting, .closed_job_postings.active, .add-new-job-posting, .cr-new-job-posting', function () {
        var html = '';
        var img = $(this).attr('class');
        html += '<div class="body-modal"></div>';
        html += '<div class="create-job-posting-popup-active"';
        if (options.leftPanel) {
            html += 'style="top:214px"';
        }
        html += '></div>';
        html += '<div class="create-job-posting-popup"';
        if (options.leftPanel) {
            html += 'style="top:214px"';
        }
        html += '>';
        html += '    <div class="popup-title">Create Job Posting</div>';
        html += '    <div class="field-name">Job Posting Name:</div>';
        html += '    <div class="input"><input type="text" class="input-field" id="job-posting-name-input"></div>';
        html += '    <div class="field-name">Seniority:</div>';
        html += '    <div class="input"><div id="seniority" class="input-field select-type">Junior</div></div>';
        html += '    <div class="field-name">Group:</div>';
        html += '    <div class="input"><div id="team-name" class="input-field select-type">Select Group</div></div>';
        html += '    <input type="hidden" id="project-name-id">';
        html += '    <div class="instraction">';
        html += '        <b>Create A Job Posting</b>';
        html += '        After adding the name of the position, you may specify exact job experience or pick the seniority level, as well as change the group it belongs to.';
        html += '    </div>';
        html += '    <div class="buttons">';
        html += '        <a class="popup-btn-cancel">Cancel</a>';
        html += '        <span>|</span>';
        html += '        <a class="popup-btn-create">Create</a>';
        html += '    </div>';
        html += '</div>';
        $('#left-create-job').after(html);
        $('#job-posting-name-input').focus();
        setTimeout(function () {
            if (img === 'closed_job_postings active') {
                var group_id = $('.closed_job_postings').attr('data-id');
                var $this = $(this);
                $('.select-list').remove();
                $('.input > div').css('z-index', 1);
                if ($this.hasClass('open')) {
                    $this.css('z-index', 1).removeClass('open');
                }
                else {
                    $.ajax({
                        url: '/group',
                        dataType: 'JSON',
                        type: 'GET',
                        data: {q: 1, group_id: group_id},
                        success: function (data) {
                            var html = '<div class="select-list">';
                            html += '<span class="first-span-job"><strong>Create New Group +</strong></span>';
                            html += '<div class="scroll-list">';
                            for (var p in data.list) {
                                html += '<span data-id="' + p + '" class="team-name-span">' + data.list[p].name + '</span>';
                            }
                            html += '</div>';
                            html += '</div>';
                            $this.closest('.select-list').remove();
                            $this.closest('.input').append(html);
                            $this.css('z-index', 4).addClass('open');
                            setTimeout(function () {
                                $('#team-name').text(data.first);
                                $('#project-name-id').val(data.id);
                            }, 100);
                        }
                    });
                }
            }
        }, 100);
    });

    $(document).on('click', '#seniority', function () {
        var $this = $(this);
        $('.select-list').remove();
        $('.input > div').css('z-index', 1);
        if ($this.hasClass('open')) {
            $this.css('z-index', 1).removeClass('open');
        }
        else {
            $this.closest('.select-list').remove();
            $this.closest('.input').append('<div class="select-list">' +
                '<div class="field-total-experience">Add Total Experience + </div>' +
                '<span class="seniority-span">Junior</span>' +
                '<span class="seniority-span">Mid-level</span>' +
                '<span class="seniority-span">Senior</span>' +
                '</div>');
            $this.css('z-index', 4).addClass('open');
        }
    });

    $(document).on('click', '.field-total-experience:not(.add_open_tx)', function () {
        var $this = $(this);
        $this.addClass('add_open_tx');
        $this.html('<input type="text" id="extra_tx" placeholder="Total Experience"><input type="button" id="extra_tx_submit" value="Add">');
        $('#extra_tx').focus();
    });

    $(document).on('keyup', '#extra_tx', function (e) {

        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            extraTX();

            return false;
        }

        var key = $(this).val();
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            $(this).val('').attr('placeholder', 'Insert only numeric!');
        }

        return false;
    });

    $(document).on('click', '#extra_tx_submit', function () {
        extraTX();
    });

    function extraTX() {
        var n = $('#extra_tx').val();
        $('#extra_tx_hidden').remove();
        if (n === '') {
            $('.field-total-experience').removeClass('add_open_tx').text('Add Total Experience + ');
        }
        else {
            $('.field-total-experience').removeClass('add_open_tx').text('Total Experience: ' + n);
            $('.create-job-posting-popup').append('<input type="hidden" id="extra_tx_hidden" value="' + n + '">');
        }
    }

    $(document).on('click', '.seniority-span', function () {
        var seniority = $(this).text();
        var tx = $('.create-job-posting-popup').find('#extra_tx_hidden').val();
        $('#seniority').text(seniority).removeClass('open');
        if (tx) {
            $('#seniority').prepend('Total Expirience: ' + tx + ' | ');
        }
        $('.select-list').remove();
    });

    $(document).on('click', '#team-name', function () {
        var $this = $(this);
        $('.select-list').remove();
        $('.input > div').css('z-index', 1);
        if ($this.hasClass('open')) {
            $this.css('z-index', 1).removeClass('open');
        }
        else {
            $.ajax({
                url: '/group',
                dataType: 'JSON',
                type: 'GET',
                data: {q: 1},
                success: function (data) {
                    var html = '<div class="select-list">';
                    html += '<span class="first-span-job"><strong>Create New Group +</strong></span>';
                    html += '<div class="scroll-list">';
                    for (var p in data) {
                        html += '<span data-id="' + data[p].id + '" class="team-name-span">' + data[p].name + '</span>';
                    }
                    html += '</div>';
                    html += '</div>';
                    $this.closest('.select-list').remove();
                    $this.closest('.input').append(html);
                    $this.css('z-index', 4).addClass('open');
                }
            });
        }
    });

    $(document).on('click', '.team-name-span', function () {
        var $this = $(this);
        var id = $this.data('id');
        var text = $this.text();
        var tn = $('#team-name');
        $('#project-name-id').val(id);
        $('.select-list').remove();
        $('.input > div').css('z-index', 1);
        tn.text(text).css('z-index', 1).removeClass('open');
    });

    $(document).on('click', '.first-span-job:not(.add_open)', function () {
        var $this = $(this);
        $this.addClass('add_open');
        $this.html('<input type="text" id="extra_project"><input type="button" id="extra_project_submit" value="Add">');
        $('#extra_project').focus();
    });

    $(document).on('click', '#extra_project_submit', function () {
        var ep = $('#extra_project');
        var tn = $('#team-name');
        if (ep.val() === '') {
            ep.attr('placeholder', 'Enter group name!')
        }
        else {
            $.ajax({
                url: '/group',
                dataType: 'JSON',
                type: 'POST',
                data: {name: ep.val(), _token: token},
                success: function (data) {
                    if (data.data == 'exist') {
                        ep.val('').attr('placeholder', 'You already have project with this name!');
                    }
                    else {
                        $('#left-side ul.with-scroll').prepend('<li class="team_name" id="' + data.id + '">' + data.name + '</li>');
                        $('.no-have, .no-have-tmp').remove();
                        $('.filter-projects-input > p').append('<a data-id="' + data.id + '">' + data.name + '</a>');
                        $('#project-name-id').val(data.id);
                        tn.text(data.name).css('z-index', 1).removeClass('open');
                        $('.select-list').remove();
                    }
                }
            });
        }
    });

    $(document).on('click', '.create-job-posting-popup .popup-btn-create', function () {
        if ($('.field-total-experience').length == 1) {
            $('.seniority-span').css('color', 'red');
            setTimeout(function () {
                $('.seniority-span').css('color', '#333333');
            }, 500);
        }
        else {
            var jp = $('#job-posting-name-input');
            var tn = $('#team-name');
            if (jp.val() === '') {
                jp.attr('placeholder', 'Please insert Job Posting Name!').focus();
            }
            else if (tn.text() === 'Select Group' || tn.text() === 'Please select Group!') {
                tn.html('<red>Please select Group!</red>');
            }
            else {
                $.ajax({
                    url: '/description/add',
                    dataType: 'JSON',
                    type: 'POST',
                    data: {seniority: $('#seniority').text(), job_posting_name: jp.val(), group_name: tn.text(), group_id: $('#project-name-id').val(), _token: token},
                    success: function (data) {
                        $('.create-job-posting-popup > div.popup-title').text('Job posting has been successfully created');
                        var el = $('.create-job-posting-popup');
                        el.find('.field-name, .input, .instraction, .buttons').remove();
                        var html = '';
                        html += '    <div class="instraction">';
                        html += '        <b>' + jp.val() + '</b>';
                        html += '        position has been successfully created.<br>';
                        html += '        Your unique assistant email for this particular job post is below.';
                        html += '        Use it when posting an announcement.';
                        html += '    </div>';
                        html += '    <br>';
                        html += '    <div class="input"><input type="text" id="team-email" class="input-field" value="' + data.email + '" readonly><a id="copy">Copy</a></div>';
                        html += '    <div class="instraction">';
                        html += '        Request the candidates to send their resumes to your recruitment assistant email and we will take care of the rest.<br><br>';
                        html += '    </div>';
                        html += '    <div class="buttons">';
                        html += '        <a class="popup-btn-ok" data-id="' + data.id + '" data-check="' + data.pid + '" data-name="' + data.pnm + '">Ok</a>';
                        html += '    </div>';
                        el.append(html);
                    }
                });
            }
        }
    });

    function JP(data) {
        $('#left-dashboard').addClass('active');
        $('#main-content > div, .hide-first-post').hide();
        removeOpenJP();
        var html = '';
        html += '<div class="jp_header">';
        html += '<ul>';
        html += '<li class="jp_header_pn">';
        html += 'Group Name:<br><b>' + data.project.name + '</b>';
        html += '</li>';
        html += '<li class="jp_header_jpn">';
        html += 'Job Posting Name:<br><b>';
        if (data.description.fields.position.length > 30) {
            html += data.description.fields.position.slice(0, 30) + '..';
        }
        else {
            html += data.description.fields.position;
        }
        html += '</b>';
        html += '</li>';
        html += '<li class="jp_header_sl">';
        html += 'Seniority Level:<br><b>' + data.description.fields.seniority + '</b>';
        html += '</li>';
        html += '<li class="jp_header_tx">';
        if (data.description.fields.total_experience && data.description.fields.total_experience != 0) {
            html += 'Total Experience:<br><b>' + data.description.fields.total_experience + '</b>';
        }
        else {
            html += 'Total Experience:<br><b>-</b>';
        }
        html += '</li>';
        html += '<li class="jp_header_e">';
        html += '<p>Email:<br><b>' + data.description.email + '</b></p>';
        html += '</li>';
        html += '<li class="edit_jp_header">';
        html += '<a></a>';
        html += '</li>';
        html += '</ul>';
        html += '</div>';
        html += '<div class="jp">';
        html += '<div class="jp_left">';
        html += '<a href="/heyling" class="abs-back"><img src="/assets/css/lib/heyling/right-back-arrow.png" alt=""></a>';
        html += '<a class="jp_1"><span></span><div></div>Skills</a>';
        html += '<a class="jp_6"><span></span><div></div>Job Responsibility</a>';
        html += '<a class="jp_2"><span></span><div></div>Education</a>';
        html += '<a class="jp_3"><span></span><div></div>Other Skills</a>';
        html += '<a class="jp_5"><span></span><div></div>Questions</a>';
        html += '</div>';
        html += '<div class="jp_right">';
        html += '<div class="empty_jp">';
        html += '<img src="/assets/css/lib/heyling/empty_jp.png" alt="">';
        html += '<h3>Welcome to ' + data.description.fields.position + ' page.</h3>';
        html += '<h5>Start by entering the required skills and responsibilities for this position. You may activate or hide the blocks on the left side of the page.';
        html += ' When candidates start applying, the skill breakdown and summary reports will be generated.</h5>';
        html += '</div>';
        html += '<div class="jp_form_1">';
        html += '<div class="jp_title">';
        html += '<span>Primary Skills</span> <i><p>Primary skills are the main or required skills expected from the candidate for this job.</p></i> ';
        html += '</div>';
        html += '<div class="jp_body">';
        html += '<div>Add primary skills and years experience:</div> ';
        html += '<div>';
        html += '<div class="div-select"><input class="select" ';
        html += 'value="" placeholder="Skill" id="ps_input"';
        html += '></div>';
        html += '<div class="div-select"><input class="select" id="ps_years_experience" ';
        html += 'placeholder="years experience"';
        html += '></div>';
        html += '</div>';
        html += '<div class="clear suggestions">Suggestions:';
        html += '<span>Java</span> <span>C#</span> <span>PHP</span> ';
        html += '</div> ';
        html += '</div>';
        html += '<div class="jp_footer">';
        for (var i in data.description.fields.skills) {
            var d = '';
            if (data.description.fields.skills[i].experience === 'not required') {

            }
            else if (data.description.fields.skills[i].experience === '< 1') {
                d = ' year';
            }
            else {
                d = ' years';
            }
            html += '<i>' + data.description.fields.skills[i].skill + ' / ' + data.description.fields.skills[i].experience + d + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i> ';
        }
        html += '</div>';
        html += '</div>';
        html += '<div class="jp_form_6">';
        html += '<div class="jp_title">';
        html += ' <span>Job Responsibility</span> <i><p>Job responsibilities are the day to day activities and other related duties for this particular job position.</p></i> ';
        html += '</div>';
        html += '<div class="jp_body">';
        html += '<div>Add requirement:</div>';
        html += '<div>';
        html += '<input type="text" class="requirement-input" placeholder="type requirements"> <a class="add-btn">Add</a>';
        html += '</div>';
        for (var r in data.description.fields.requirements) {
            html += '<div class="qr-txt">' + data.description.fields.requirements[r] + ' <a class="qr-sort"></a> <img src="/assets/css/lib/heyling/x.png" alt=""></div>';
        }
        html += '</div>';
        html += '<div class="jp_footer_empty"></div>';
        html += '</div>';
        html += '<div class="jp_form_2">';
        html += '<div class="jp_title">';
        html += ' <span>Education</span> <i><p>Education can be either a specific educational institution that you want to be a mandatory requirement or a direction or a degree requirement such as "technical", masters or bachelor degree.</p></i>';
        html += '</div>';
        html += '<div class="jp_body">';
        html += '<div>Add education:</div>';
        html += '<div class="select-education">';
        html += '<input class="select" id="select_education" placeholder="education">';
        html += '</div>';
        html += '</div>';
        html += '<div class="jp_footer">';
        for (var e in data.description.fields.education) {
            html += '<i>' + data.description.fields.education[e] + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i> ';
        }
        html += '</div>';
        html += '</div>';
        html += '<div class="jp_form_3">';
        html += '<div class="jp_title">';
        html += ' <span>Other Skills</span> <i><p>Other skills are the nice to have or preferred skills that you think would increase candidate’s chances to be interviewed for this position.</p></i> ';
        html += '</div>';
        html += '<div class="jp_body">';
        html += '<div>Add other skills and years experience:</div> ';
        html += '<div>';
        html += '<div class="div-os-select"><input class="select" ';
        html += 'value="" placeholder="Skill" id="os_input"';
        html += '></div>';
        html += '<div class="div-select"><input id="os_years_experience"';
        html += 'placeholder="years experience"';
        html += '></div>';
        html += '</div>';
        html += '<div class="clear suggestions">Suggestions:';
        html += '<span>Java</span> <span>C#</span> <span>PHP</span> ';
        html += '</div> ';
        html += '</div>';
        html += '<div class="jp_footer">';
        for (var o in data.description.fields.other_skills) {
            html += '<i>' + data.description.fields.other_skills[o].skill + ' / ' + data.description.fields.other_skills[o].experience + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i> ';
        }
        html += '</div>';
        html += '</div>';
        html += '<div class="jp_form_5">';
        html += '<div class="jp_title">';
        html += ' <span>Questions</span> <i><p>Type here the questions you want the candidate to answer as part of the pre-screening process. The system will ask the candidates your questions and include the answers, or lack of those, in the summary report.</p></i> ';
        html += '</div>';
        html += '<div class="jp_body">';
        html += '<div>Add question:</div>';
        html += '<div>';
        html += '<input type="text" id="question-ac" class="question-input" placeholder="type your question here"> <a class="add-btn">Add</a>';
        html += '</div>';
        for (var q in data.description.fields.questions) {
            html += '<div class="qr-txt">Question: ' + data.description.fields.questions[q] + ' <a class="qr-sort"></a> <img src="/assets/css/lib/heyling/x.png" alt=""></div>';
        }
        html += '</div>';
        html += '<div class="jp_footer_empty"></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="save_panel">';
        if (data.job_applications_count) {
            html += '<input type="button" class="bottom-btn" value="Summary Report">';
            html += '<input type="button" class="bottom-btn" value="Breakdown Report">';
            html += '<a class="sp-btn-remove-dis">Remove Job Posting</a>';
        }
        else {
            html += '<input type="button" class="bottom-btn disabled-btn" value="Summary Report">';
            html += '<input type="button" class="bottom-btn disabled-btn" value="Breakdown Report">';
            html += '<a class="sp-btn-remove">Remove Job Posting</a>';
        }
        if (data.user.upload_button) {
            html += '<div class="up-btn">';
            html += '<form id="bulk_form" action="" method="post" enctype="multipart/form-data"><input type="file" name="bulk_file[]" id="bulk_file" multiple>Upload [.zip, .pdf, .doc, .docx, .rtf]</form>';
            html += '</div>';
        }
        html += '</div>';
        $('.center-loader').remove();
        $('.for-white-link a[data-id="' + data.description._id.$oid + '"]').addClass('active');
        $('#main-content').attr('data-jp', data.description._id.$oid).append(html);
        setTimeout(function () {
            for (var af in data.description.active_forms) {
                $('.jp_left a.jp_' + data.description.active_forms[af]).click();
            }
        }, 250);
    }

    $(document).on('keyup', '#question-ac', function () {
        var $this = $(this);
        var str = $this.val();
        $('.q-ac').remove();
        $.ajax({
            url: '/heyling/ajax/ql',
            dataType: 'JSON',
            type: 'POST',
            data: {str: str},
            success: function (data) {
                var html = '<div class="q-ac"><ul>';
                var loop = false;
                for (var i in data) {
                    html += '<li>' + data[i] + '</li>';
                    loop = true;
                }
                html += '</ul></div>';
                if (loop) {
                    $this.after(html);
                }
            }
        });
    });

    $(document).on('click', 'body', function () {
        $('.q-ac').remove();
    });

    $(document).on('click', '.q-ac li', function () {
        var txt = $(this).text();
        $('#question-ac').val(txt);
    });

    $(document).on('change', '#bulk_file', function () {
        var file = $(this);
        var ext = file.val().split('.');
        if (ext[ext.length - 1] === 'zip' ||
            ext[ext.length - 1] === 'pdf' ||
            ext[ext.length - 1] === 'doc' ||
            ext[ext.length - 1] === 'docx' ||
            ext[ext.length - 1] === 'rtf' ||
            ext[ext.length - 1] === 'ZIP' ||
            ext[ext.length - 1] === 'PDF' ||
            ext[ext.length - 1] === 'DOC' ||
            ext[ext.length - 1] === 'DOCX' ||
            ext[ext.length - 1] === 'RTF') {
            $('#bulk_form').append('<input type="hidden" name="job_description_id" value="' + $('#main-content').attr('data-jp') + '">');
            $('#bulk_form').submit();
            $('body').append('<div class="block-body"><img src="/assets/css/lib/heyling/logo.png" alt=""></div>');
        }
        else {
            alert('The file format is not valid.');
        }
    });

    $(document).on('click', '.popup-btn-ok', function () {
        var id = $(this).attr('data-id');
        var pi = $(this).attr('data-check');
        var pn = $(this).attr('data-name');
        var mc = $('#main-content');
        $('.body-modal, .create-job-posting-popup, .create-job-posting-popup-active').remove();
        $.ajax({
            url: '/description/edit',
            dataType: 'JSON',
            type: 'POST',
            data: {id: id, _token: token},
            success: function (data) {
                JP(data);
                if (pi == $('.closed_job_postings.active').attr('data-id')) {
                    $('.for-white-link').prepend('<a data-id="' + data.description._id.$id + '" class="white-link">' + data.description.fields.position + '</a>');
                }
                else {
                    $('.active_job_posting, .closed_job_postings, .for-white-link').remove();
                    $('.separate').after('<a class="active_job_posting"><span></span>' + pn + '</a> <a class="closed_job_postings active" data-id="' + pi + '">Job Postings <img src="/assets/css/lib/heyling/plus.png" alt=""></a>');
                    var $this = $('.closed_job_postings');
                    $.ajax({
                        url: '/description/get',
                        dataType: 'JSON',
                        type: 'POST',
                        data: {id: pi, _token: token},
                        success: function (data) {
                            var html = '<div class="for-white-link">';
                            var links = 0;
                            for (var p in data) {
                                html += '<a data-id="' + p + '" class="white-link">' + data[p].fields.position + '</a>';
                                links++;
                            }
                            html += '</div>';
                            $this.after(html);
                            sizeCorrections();
                            var str_10 = $('a.active_job_posting').text();
                            if (str_10.length > 7) {
                                str_10 = str_10.slice(0, 6) + '..';
                            }
                            $('#left-side').append('<a class="remove_project">Remove "' + str_10.trim() + '" Group <img src="/assets/css/lib/heyling/trush-project.png" alt=""></a>');
                        }
                    });
                }
                setTimeout(function () {
                    $('.for-white-link > .white-link').removeClass('active');
                    $('.for-white-link > a:first-child').addClass('active');
                }, 200);
            }
        });
    });

    $(document).on('click', '#copy', function () {
        var te = $('#team-email');
        te.select();
        document.execCommand("Copy");
        $(this).closest('.input').append('<div class="copied">Copied!</div>');
        setTimeout(function () {
            $('.copied').remove();
        }, 1500);
    });

    $(document).on('click', '.sp-btn-remove', function () {
        $(this).closest('div').append('<div class="areyousure"> Please confirm. <a>Delete</a> <a>Cancel</a></div>');
    });

    $(document).on('click', '.areyousure a', function () {
        if ($(this).text() === 'Cancel') {
            $('.areyousure').remove();
        }
        else {
            $.ajax({
                url: '/description/remove_job_posting',
                type: 'POST',
                data: {id: $('#main-content').attr('data-jp'), _token: token},
                success: function () {
                    window.location.href = '/home';
                }
            });
        }
    });

    $(document).on('click', '#team-email', function () {
        $(this).select();
    });

    $(document).on('click', '.jp_form_1 .suggestions span', function () {
        var $this = $(this);
        $this.closest('.jp_body').find('input.select').val($this.text());
        var txt = $this.text();
        $('#ps_input').val(txt);
        $('.option-up').remove();
        $('#ps_years_experience').show().focus();
        var html = '<div class="options">';
        for (var i = 0; i < skillsExp.length; i++) {
            html += '<a>' + skillsExp[i] + '</a>';
        }
        html += '</div>';
        $('#ps_years_experience').after(html).addClass('active');
    });

    $(document).on('click', '.jp_form_3 .suggestions span', function () {
        var $this = $(this);
        $this.closest('.jp_body').find('input.select').val($this.text());
        var txt = $this.text();
        $('#os_input').val(txt);
        $('.option-up').remove();
        $('#os_years_experience').show().focus();
        var html = '<div class="options">';
        for (var i = 0; i < skillsExp.length; i++) {
            html += '<a>' + skillsExp[i] + '</a>';
        }
        html += '</div>';
        $('#os_years_experience').after(html).addClass('active');
    });

    var skillsList = [];
    var skillsDetails = [];

    $.ajax({
        url: '/home/all_skills',
        dataType: 'JSON',
        type: 'POST',
        data: {_token: token},
        success: function (data) {
            $.each(data, function (key, item) {
                skillsList.push(item);
            });
            skillsDetails = data;
        }
    });

    var skillsExp = [
        'not required', '< 1', '1-2', '2-3', '3-5', '5-7', '7-10', '> 10'
    ];

    var educationList = [
        'Yerevan State University',
        'National University of Architecture and Construction of Armenia',
        'Yerevan State Medical University',
        'Armenian State Pedagogical University',
        'Komitas State Conservatory of Yerevan',
        'Armenian National Agrarian University',
        'National Polytechnic University of Armenia',
        'Yerevan Brusov State University of Languages and Social Sciences',
        'Yerevan State Institute of Theatre and Cinematography',
        'Armenian State Institute of Physical Culture',
        'Yerevan State Academy of Fine Arts',
        'Armenian State University of Economics',
        'Crisis Management State Academy',
        'Outside Yerevan',
        'Shirak State University',
        'Goris State University',
        'Vanadzor State University',
        'Gavar State University'
    ];

    var lngList = [
        'English',
        'Russian',
        'French',
        'Armenian',
        'Italian'
    ];

    $(document).on('keyup', '#select_education', function (e) {
        var keyCode = e.keyCode || e.which;
        var $this = $(this);
        $this.closest('.select-education').find('.option-ed').remove();
        var w = +30 + $this.width();
        var html = '<div class="option-ed" style="width: ' + w + 'px">';
        for (var i = 0; i < educationList.length; i++) {
            if (educationList[i].match(new RegExp($this.val(), "i"))) {
                html += '<a>' + educationList[i] + '</a>';
            }
        }
        html += '</div>';
        $this.closest('.select-education').append(html);
        if (keyCode === 13) {
            if ($this.val() !== '') {
                addEducationEnter($this.val());
            }
        }
    });

    $(document).on('keyup', '#select_lng', function (e) {
        var keyCode = e.keyCode || e.which;
        var $this = $(this);
        $this.closest('.select-lng').find('.option-lng').remove();
        var w = +30 + $this.width();
        var html = '<div class="option-lng" style="width: ' + w + 'px">';
        for (var i = 0; i < lngList.length; i++) {
            if (lngList[i].match(new RegExp($this.val(), "i"))) {
                html += '<a>' + lngList[i] + '</a>';
            }
        }
        html += '</div>';
        $this.closest('.select-lng').append(html);
        if (keyCode === 13) {
            if ($this.val() !== '') {
                addLngEnter($this.val());
            }
        }
    });

    function addEducationEnter(txt) {
        $('.option-ed').remove();
        $('.jp_form_2 .jp_footer').append('<i>' + txt + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i>');
        $('#select_education').val('').attr('placeholder', 'education');
        var form_2 = [];
        $('.jp_form_2 .jp_footer i').each(function () {
            form_2.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_education',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    edu: form_2,
                    _token: token
                }
            });
        }, 100);
    }

    $(document).on('click', '.jp_form_2 .jp_footer i img', function () {
        $(this).closest('i').remove();
        var form_2 = [];
        $('.jp_form_2 .jp_footer i').each(function () {
            form_2.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_education',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    edu: form_2,
                    _token: token
                }
            });
        }, 100);
    });

    $(document).on('click', '.option-ed a', function () {
        var $this = $(this);
        var txt = $this.text();
        addEducationEnter(txt);
    });

    $(document).on('click', '.option-lng a', function () {
        var $this = $(this);
        var txt = $this.text();
        addLngEnter(txt);
    });

    $(document).on('click', '.jp_form_5 .add-btn', function () {
        addQR($('.question-input'), 'Please Insert Your Question', 'Question: ');
    });

    $(document).on('click', '.jp_form_6 .add-btn', function () {
        addQR($('.requirement-input'), 'Please Insert Job Responsibility', '');
    });

    $(document).on('keyup', '.question-input, .requirement-input', function (e) {
        var $this = $(this);
        var keyCode = e.keyCode || e.which;
        var q = '';
        if ($this.attr('class') === 'question-input') {
            q = 'Question: ';
        }
        if (keyCode === 13 && $this.val() !== '') {
            addQR($this, '', q);
        }
    });

    function addQR(qi, msg, q) {
        if (qi.val() == '') {
            qi.addClass('question-error').attr('placeholder', msg);
        }
        else {
            qi.closest('div').after('<div class="qr-txt">' + q + qi.val() + ' <a class="qr-sort"></a> <img src="/assets/css/lib/heyling/x.png" alt=""></div>');
            qi.val('');
        }
        var form_5 = [], form_6 = [];
        $('.jp_form_5 .jp_body .qr-txt').each(function () {
            form_5.push($(this).text());
        });
        $('.jp_form_6 .jp_body .qr-txt').each(function () {
            form_6.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_qr',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    jp5: form_5,
                    jp6: form_6,
                    _token: token
                }
            });
        }, 100);
    }

    function setSkillToFooterBox(experience) {
        var skill = $('#ps_input').val();
        $('.jp_form_1 .jp_footer i').each(function () {
            var txt = $(this).text();
            txt = txt.split('/');
            txt = txt[0].trim();
            if (skill.toLowerCase() == txt.toLowerCase()) {
                $(this).remove();
            }
        });
        var form_1 = [];
        if (skill !== '' && experience !== '') {
            if (experience === 'not required') {

            }
            else if (experience === '< 1') {
                experience = '< 1 year';
            }
            else {
                experience = experience + ' years';
            }
            if (skill.indexOf(',') > -1) {
                var arr = skill.split(',');
                for (var prop in arr) {
                    if (arr[prop] != '' || arr[prop] != ' ') {
                        $('.jp_form_1 .jp_footer').prepend('<i class="yellow-bg">' + arr[prop] + ' / ' + experience + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i>');
                    }
                }
            }
            else {
                $('.jp_form_1 .jp_footer').prepend('<i class="yellow-bg">' + skill + ' / ' + experience + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i>');
            }

            $('#ps_input').val('').attr('placeholder', 'Skill');
            $('#ps_years_experience').val('').hide();
            $('.jp_form_1 .options').remove();

            $('.jp_form_1 .jp_footer i').each(function () {
                form_1.push($(this).text());
            });
            $('.jp_form_1 .suggestions span').remove();
            if(skillsDetails[skill]) {
                $.each(skillsDetails[skill], function (key, value) {
                    $('.jp_form_1 .suggestions').append('<span>' + value + '</span>');
                });
            }
            setTimeout(function () {
                $.ajax({
                    url: '/description/update_job_posting_skill',
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        id: $('#main-content').attr('data-jp'),
                        jp1: form_1,
                        sk: 'sk',
                        _token: token
                    }
                });
            }, 100);
            setTimeout(function () {
                $('#ps_input').focus();
            }, 200);
        }
    }

    function setOSkillToFooterBox(experience) {
        var skill = $('#os_input').val();
        $('.jp_form_3 .jp_footer i').each(function () {
            var txt = $(this).text();
            txt = txt.split('/');
            txt = txt[0].trim();
            if (skill.toLowerCase() == txt.toLowerCase()) {
                $(this).remove();
            }
        });
        var form_3 = [];
        if (skill !== '' && experience !== '') {
            if (experience === 'not required') {

            }
            else if (experience === '< 1') {
                experience = '< 1 year';
            }
            else {
                experience = experience + ' years';
            }
            if (skill.indexOf(',') > -1) {
                var arr = skill.split(',');
                for (var prop in arr) {
                    if (arr[prop] != '' || arr[prop] != ' ') {
                        $('.jp_form_3 .jp_footer').prepend('<i class="yellow-bg">' + arr[prop] + ' / ' + experience + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i>');
                    }
                }
            }
            else {
                $('.jp_form_3 .jp_footer').prepend('<i class="yellow-bg">' + skill + ' / ' + experience + ' <img src="/assets/css/lib/heyling/x.png" alt=""></i>');
            }
            $('#os_input').val('').attr('placeholder', 'Skill');
            $('#os_years_experience').val('').hide();
            $('.jp_form_3 .options').remove();
            $('.jp_form_3 .jp_footer i').each(function () {
                form_3.push($(this).text());
            });
            $('.jp_form_3 .suggestions span').remove();
            if(skillsDetails[skill]) {
                $.each(skillsDetails[skill], function (key, value) {
                    $('.jp_form_3 .suggestions').append('<span>' + value + '</span>');
                });
            }
            setTimeout(function () {
                $.ajax({
                    url: '/description/update_job_posting_skill',
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        id: $('#main-content').attr('data-jp'),
                        jp1: form_3,
                        sk: 'os',
                        _token: token
                    }
                });
            }, 100);
            setTimeout(function () {
                $('#os_input').focus();
            }, 200);
        }
    }

    $(document).on('click', '.jp_form_1 .jp_footer i img', function () {
        $(this).closest('i').remove();
        var form_1 = [];
        $('.jp_form_1 .jp_footer i').each(function () {
            form_1.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_skill',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    jp1: form_1,
                    sk: 'sk',
                    _token: token
                }
            });
        }, 100);
    });

    $(document).on('click', '.jp_form_3 .jp_footer i img', function () {
        $(this).closest('i').remove();
        var form_3 = [];
        $('.jp_form_3 .jp_footer i').each(function () {
            form_3.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_skill',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    jp1: form_3,
                    sk: 'os',
                    _token: token
                }
            });
        }, 100);
    });

    $(document).on('click', '.jp_form_1 .option-up a', function () {
        var $this = $(this);
        var txt = $this.text();
        $('#ps_input').val(txt);
        $('.option-up').remove();
        $('#ps_years_experience').show().focus();
        var html = '<div class="options">';
        for (var i = 0; i < skillsExp.length; i++) {
            html += '<a>' + skillsExp[i] + '</a>';
        }
        html += '</div>';
        $('#ps_years_experience').after(html).addClass('active');
    });

    $(document).on('click', '.jp_form_3 .option-up a', function () {
        var $this = $(this);
        var txt = $this.text();
        $('#os_input').val(txt);
        $('.option-up').remove();
        $('#os_years_experience').show().focus();
        var html = '<div class="options">';
        for (var i = 0; i < skillsExp.length; i++) {
            html += '<a>' + skillsExp[i] + '</a>';
        }
        html += '</div>';
        $('#os_years_experience').after(html).addClass('active');
    });

    $(document).on('keydown keyup', '#ps_input', function (e) {
        selectSkill($(this), e);
        if (index > 5) {
            $('.jp_form_1 .option-up').scrollTop(index * 26);
        }
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 || keyCode === 9) {
            $('.jp_form_1 .options a.hover-focus').click();
        }
    });

    $(document).on('keydown keyup', '#os_input', function (e) {
        selectOSkill($(this), e);
        if (index > 5)
            $('.jp_form_3 .option-up').scrollTop(index * 26);
    });

    $(document).on('keyup', '#ps_input', function (e) {
        var $items = $('.jp_form_1 .option-up a');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 40 || keyCode === 38) {
            e.preventDefault();
            if (keyCode === 38 && index > 0)
                index--;
            if (keyCode === 40 && index < $items.length - 1)
                index++;
            if (!~index)
                index = 0;
            $items.eq(index).addClass('hover-focus');
        }
        else {
            index = -1;
        }
    });

    $(document).on('click', '.jp_form_1 .options a', function () {
        setSkillToFooterBox($(this).text());
    });

    $(document).on('keydown keyup', '#ps_years_experience', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 || keyCode === 9) {
            $('.jp_form_1 .options a.hover-focus').click();
        }
    });

    $(document).on('keyup', '#ps_years_experience', function (e) {
        var $items = $('.jp_form_1 .options a');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 40 || keyCode === 38) {
            e.preventDefault();
            $items.eq(indexExp).removeClass('hover-focus');
            if (keyCode === 38 && indexExp > 0)
                indexExp--;
            if (keyCode === 40 && indexExp < $items.length - 1)
                indexExp++;
            if (!~indexExp)
                indexExp = 0;
            $items.eq(indexExp).addClass('hover-focus');
        }
        else {
            indexExp = -1;
        }
    });

    function selectOSkill($this, e) {
        var keyCode = e.keyCode || e.which;
        var html = '';
        var i = 0;
        if (keyCode === 13 || keyCode === 9) {
            if ($('.jp_form_3 .option-up a.hover-focus').text() !== '') {
                $('#os_input').val($('.jp_form_3 .option-up a.hover-focus').text());
            }
            $('.option-up').remove();
            html = '<div class="options">';
            for (i = 0; i < skillsExp.length; i++) {
                html += '<a>' + skillsExp[i] + '</a>';
            }
            html += '</div>';
            $('#os_years_experience').show().focus();
        }
        else {
            $this.closest('.div-os-select').find('.option-up').remove();
            var w = +30 + $this.width();
            html = '<div class="option-up" style="width: ' + w + 'px">';
            for (i = 0; i < skillsList.length; i++) {
                if (skillsList[i].match(new RegExp($this.val(), "i"))) {
                    html += '<a>' + skillsList[i] + '</a>';
                }
            }
            html += '</div>';
        }
        $this.closest('.div-os-select').append(html);
    }

    function selectSkill($this, e) {
        var keyCode = e.keyCode || e.which;
        var html = '';
        var i = 0;
        if (keyCode === 13 || keyCode === 9) {
            if ($('.jp_form_1 .option-up a.hover-focus').text() !== '') {
                $('#ps_input').val($('.jp_form_1 .option-up a.hover-focus').text());
            }
            $('.option-up').remove();
            html = '<div class="options">';
            for (i = 0; i < skillsExp.length; i++) {
                html += '<a>' + skillsExp[i] + '</a>';
            }
            html += '</div>';
            $('#ps_years_experience').show().focus();
        }
        else {
            $this.closest('.div-select').find('.option-up').remove();
            var w = +30 + $this.width();
            html = '<div class="option-up" style="width: ' + w + 'px">';
            var search_string = $this.val();
            search_string = search_string.replace(/[\+]/g, "");
            var array_ac = [];
            $('.jp_form_1 .jp_footer > i').each(function () {
                var et = $(this).text().split('/');
                array_ac.push(et["0"].trim());
            });
            for (i = 0; i < skillsList.length; i++) {
                if (skillsList[i].match(new RegExp(search_string, "i")) && $.inArray(skillsList[i], array_ac) === -1) {
                    html += '<a>' + skillsList[i] + '</a>';
                }
            }
            html += '</div>';
        }
        $this.closest('.div-select').append(html);
    }

    var index = -1, indexExp = -1, index2 = -1, indexExp2 = -1, indexEdu = -1, indexLng = -1;

    $(document).on('keydown keyup', '#select_education', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 && $('.jp_form_2 .option-ed a.hover-focus').text()) {
            $(this).val($('.jp_form_2 .option-ed a.hover-focus').text());
        }
        else if (indexEdu > 5) {
            $('.jp_form_2 .option-ed').scrollTop(indexEdu * 26);
        }
    });

    $(document).on('keyup', '#select_education', function (e) {
        var $items = $('.jp_form_2 .option-ed a');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 40 || keyCode === 38) {
            e.preventDefault();
            if (keyCode === 38 && indexEdu > 0)
                indexEdu--;
            if (keyCode === 40 && indexEdu < $items.length - 1)
                indexEdu++;
            if (!~indexEdu)
                indexEdu = 0;
            $items.eq(indexEdu).addClass('hover-focus');
        }
        else {
            indexEdu = -1;
        }
    });

    $(document).on('keyup', 'body', function (e) {
        e.preventDefault();
        var keyCode = e.keyCode || e.which;
        if (keyCode === 27) {
            $('.popup-btn-cancel').click();
            $('.option-up, .option-ed, .option-lng').remove();
        }
        else if (keyCode === 13) {
            if ($('.create-job-posting-popup-active').length == 1 && $('#seniority.input-field.select-type.open').length == 1) {
                var seniority = $('.select-list .sel').text();
                var tx = $('.create-job-posting-popup').find('#extra_tx_hidden').val();
                $('#seniority').text(seniority).removeClass('open');
                if (tx) {
                    $('#seniority').prepend('Total Expirience: ' + tx + ' | ');
                }
                $('.select-list').remove();
            }
            else if ($('.create-job-posting-popup-active').length == 1) {
                $('.create-job-posting-popup .popup-btn-create').click();
            }
            else if ($('.popup-btn-ok').length == 1 && !$('#seniority.input-field.select-type.open').length) {
                $('.popup-btn-ok').click();
            }
        }
    });

    $(document).on('keyup', '#os_input', function (e) {
        var $items = $('.jp_form_3 .option-up a');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 40 || keyCode === 38) {
            e.preventDefault();
            if (keyCode === 38 && index2 > 0)
                index2--;
            if (keyCode === 40 && index2 < $items.length - 1)
                index2++;
            if (!~index2)
                index2 = 0;
            $items.eq(index2).addClass('hover-focus');
        }
        else {
            index2 = -1;
        }
    });

    $(document).on('click', '.jp_form_3 .options a', function () {
        setOSkillToFooterBox($(this).text());
    });

    $(document).on('keydown keyup', '#os_input', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 || keyCode === 9) {
            $('.jp_form_3 .options a.hover-focus').click();
        }
    });

    $(document).on('keydown keyup', '#os_years_experience', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 || keyCode === 9) {
            $('.jp_form_3 .options a.hover-focus').click();
        }
    });

    $(document).on('keyup', '#os_years_experience', function (e) {
        var $items = $('.jp_form_3 .options a');
        var keyCode = e.keyCode || e.which;
        if (keyCode === 40 || keyCode === 38) {
            e.preventDefault();
            $items.eq(indexExp2).removeClass('hover-focus');
            if (keyCode === 38 && indexExp2 > 0)
                indexExp2--;
            if (keyCode === 40 && indexExp2 < $items.length - 1)
                indexExp2++;
            if (!~indexExp2)
                indexExp2 = 0;
            $items.eq(indexExp2).addClass('hover-focus');
        }
        else {
            indexExp2 = -1;
        }
    });

    $(document).on('click', '#left-create-team, .btn.btn-create-team, .groups_add_btn, .add-new-group, .link-group-create', function () {
        var html = '';
        html += '<div class="body-modal"></div>';
        html += '<div class="create-team-popup-active"';
        if (options.leftPanel) {
            html += ' style="top:149px"';
        }
        html += '></div>';
        html += '<div class="create-team-popup"';
        if (options.leftPanel) {
            html += ' style="top:149px"';
        }
        html += '>';
        html += '    <div class="popup-title">Create Group</div>';
        html += '    <div class="field-name">Group Name:</div>';
        html += '    <div class="input"><input type="text" class="input-field" id="new-team-name"></div>';
        html += '    <div class="instraction">';
        html += '        <b>Create A Group</b>';
        html += '        Enter the name of the group. Usually this field reflects an account name, department name, group name or any other structure you prefer to work with.';
        html += '        All job postings created under this group will reflect on system dashboards and reports.';
        html += '    </div>';
        html += '    <div class="buttons">';
        html += '        <a class="popup-btn-cancel">Cancel</a>';
        html += '        <span>|</span>';
        html += '        <a class="popup-btn-create">Create</a>';
        html += '    </div>';
        html += '</div>';
        $('#left-create-team').after(html);
        $('#new-team-name').focus();
    });

    $(document).on('click', '.create-team-popup .popup-btn-cancel, .create-job-posting-popup .popup-btn-cancel, .body-modal', function () {
        $('.body-modal, .create-team-popup, .create-team-popup-active, .create-job-posting-popup, .create-job-posting-popup-active').remove();
    });

    function createNewProject() {
        var name = $('#new-team-name');
        if (name.val() === '') {
            name.attr('placeholder', 'Enter group name!');
        }
        else {
            $.ajax({
                url: '/group',
                dataType: 'JSON',
                type: 'POST',
                data: {name: name.val(), _token: token},
                success: function (data) {
                    if (data.data == 'exist') {
                        name.val('').attr('placeholder', 'You already have project with this name!');
                    }
                    else {
                        if ($('#left-teams').hasClass('left-teams-pointer')) {
                            $('#left-side ul.with-scroll').prepend('<li class="team_name" id="' + data.id + '" style="display: none;">' + data.name + '</li>');
                        }
                        else {
                            $('#left-side ul.with-scroll').prepend('<li class="team_name" id="' + data.id + '">' + data.name + '</li>');
                        }
                        $('.no-have, .no-have-tmp').remove();
                        $('.filter-projects-input > p').append('<a data-id="' + data.id + '">' + data.name + '</a>');
                        $('.body-modal, .create-team-popup, .create-team-popup-active, .create-job-posting-popup, .create-job-posting-popup-active').remove();
                    }
                }
            });
        }
    }

    $(document).on('click', '.create-team-popup .popup-btn-create', function () {
        createNewProject();
    });

    $(document).on('click', '.filter-projects-input > p > a', function () {
        var $this = $(this);
        var text_project = $this.text();
        var text_skill = $('.filter-skills-input > div').text();
        var id = $this.data('id');
        $('.filter-projects-input > p').hide();
        showAppList(id, text_project, text_skill, $('.filter-search input').val());
        $('.groups_add_btn').remove();
    });

    $(document).on('click', '.filter-projects-input > div', function () {
        $('.filter-projects-input > p').toggle();
    });

    $(document).on('click', '.filter-skills-input > div', function () {
        if ($(this).text() !== 'No skills') {
            $('.filter-skills-input > p').toggle();
        }
    });

    $(document).on('click', '.filter-search a', function () {
        if ($('.filter-search input').val() !== '') {
            var text_skill = $('.filter-skills-input > div').text();
            var text_project = $('.filter-projects-input > div').text();
            var id = $('.filter-projects-input > div').attr('data-id');
            if (!!id) {

            }
            else {
                id = null;
            }
            showAppList(id, text_project, text_skill, $('.filter-search input').val());
        }
    });

    $(document).on('keyup', '.filter-search input', function () {
        if ($(this).val() === '') {
            var text_skill = $('.filter-skills-input > div').text();
            var text_project = $('.filter-projects-input > div').text();
            var id = $('.filter-projects-input > div').attr('data-id');
            if (!!id) {

            }
            else {
                id = null;
            }
            showAppList(id, text_project, text_skill, '');
        }
    });

    $(document).on('keyup', '.filter-search input', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13 && $(this).val() !== '') {
            var text_skill = $('.filter-skills-input > div').text();
            var text_project = $('.filter-projects-input > div').text();
            var id = $('.filter-projects-input > div').attr('data-id');
            if (!!id) {

            }
            else {
                id = null;
            }
            showAppList(id, text_project, text_skill, $(this).val(), $('.filter-search input').val());
        }
    });

    $(document).on('click', '.filter-skills-input > p > a', function () {
        var $this = $(this);
        var text_skill = $this.text();
        var text_project = $('.filter-projects-input > div').text();
        var id = $('.filter-projects-input > div').attr('data-id');
        $('.filter-skills-input > p').hide();
        if (text_skill === 'All' && text_project === 'All') {
            id = 0;
        }
        showAppList(id, text_project, text_skill, $('.filter-search input').val());
    });

    $(document).on('click', '.filter-hidden-input > p > a', function () {
        var $this = $(this);

    });

    $(document).on('click', '.filter-hidden-input', function () {
        $('.filter-hidden-input p').toggle();
    });

    $(document).on('click', '.jp_title', function () {
        var $this = $(this);
        if ($this.hasClass('active')) {
            $this.closest('div[class^=jp_form_]').find('.jp_body, .jp_footer, .jp_footer_empty').show();
            $this.removeClass('active');
        }
        else {
            $this.closest('div[class^=jp_form_]').find('.jp_body, .jp_footer, .jp_footer_empty').hide();
            $this.addClass('active');
        }
    });

    $(document).on('click', '.jp_form_1, .jp_form_2, .jp_form_3', function () {
        var $this = $(this);
        if ($this.find('.jp_title').hasClass('active')) {
            var st = '';
            $this.find('.jp_footer i').each(function () {
                st += $(this).text() + ' ';
            });
            $this.find('.jp_title').append('<em>' + st + '</em><img src="/assets/css/lib/heyling/title-bg.png" alt="">');
        }
        else {
            $this.find('.jp_title > em, .jp_title > img').remove();
        }
    });

    $(document).on('click', '.jp_form_4, .jp_form_5, .jp_form_6', function () {
        var $this = $(this);
        if ($this.find('.jp_title').hasClass('active')) {
            var st = '';
            $this.find('.qr-txt').each(function () {
                st += $(this).text() + ' ';
            });
            $this.find('.jp_title').append('<em>' + st + '</em><img src="/assets/css/lib/heyling/title-bg.png" alt="">');
        }
        else {
            $this.find('.jp_title > em, .jp_title > img').remove();
        }
    });

    $(document).on('click', '.jp_left a', function () {
        $('.empty_jp').hide();
        var $this = $(this);
        $this.toggleClass('active');
        var jp = $this.attr('class').substr(3, 1);
        var form_opened = [];
        if ($this.hasClass('active')) {
            $('.jp_form_' + jp).show();
        }
        else {
            $('.jp_form_' + jp).hide();
        }
        $('.jp_left a.active').each(function () {
            form_opened.push($(this).attr('class'))
        });
        $.ajax({
            url: '/description/update_jp',
            dataType: 'JSON',
            type: 'POST',
            data: {
                id: $('#main-content').attr('data-jp'),
                st: form_opened,
                _token: token
            },
            success: function (data) {
                switch (jp) {
                    case '1':
                        $('#ps_input').focus();
                        break;
                    case '2':
                        $('#select_education').focus();
                        break;
                    case '3':
                        $('#os_input').focus();
                        break;
                    case '5':
                        $('#question-ac').focus();
                        break;
                    case '6':
                        $('.jp_form_6 .requirement-input').focus();
                        break;
                }
            }
        });
    });

    $(document).on('click', '#left-dashboard', function () {
        $('#main-content > div').show();
        $('.for-white-link a').removeClass('active');
        $('#main-content').removeAttr('data-jp');
        $('.save_panel, .jp_header, .jp').remove();
        $(this).removeClass('active');
        $('#left-teams, .team_name').show();
        $('.active_job_posting, .closed_job_postings, .for-white-link').remove();
        $('.filter-projects-input > div').attr('data-id', 0).text('All');
        $('.filter-skills-input > div').attr('data-id', 0).text('All');
    });

    $(document).on('click', '.bottom-btn:not(.disabled-btn)', function () {
        var id = $('#main-content').attr('data-jp');
        options.backPath = 'job-posting';
        allReports(id, $(this).val());
    });

    $(document).on('click', '.filter-ps-input', function () {
        $('.filter-ps-input p').toggle();
    });

    $(document).on('click', '.filter-ps-input p a', function () {
        $('.filter-ps-input div').text($(this).text());
        filterBox();
    });

    $(document).on('click', '.filter-hidden-input p a', function () {
        var txt = $(this).text();
        $('.filter-hidden-input div').text(txt);
        if (txt === 'Hide') {
            $('.class-hide-row').hide();
            $('.class-show-row').show();
        }
        else if (txt === 'Show') {
            $('.class-hide-row').show();
            $('.class-show-row').hide();
        }
        else {
            $('.class-hide-row').show();
            $('.class-show-row').show();
        }
    });

    $(document).on('click', '.filter-summary-skills-input', function () {
        $('.filter-summary-skills-input p').toggle();
    });

    $(document).on('click', '.filter-summary-skills-input p a', function () {
        $('.filter-summary-skills-input div').text($(this).text());
        filterBox();
    });

    $(document).on('keyup', '.filter-summary-search input', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            filterBox();
        }
    });

    $(document).on('click', '.filter-summary-search a', function () {
        filterBox();
    });

    function filterBox() {
        var arr = {
            PrimarySkills: $('.filter-ps-input>div').text(),
            AllSkills: $('.filter-summary-skills-input>div').text(),
            TxtSearch: $('.filter-summary-search input').val()
        };
        var id = $('#main-content').attr('data-id');
        var sr = 'Summary Report';
        if($('.asbr.active').length) {
            sr = 'Breakdown Report';
        }
        else {
        }
        $('.summary_report').remove();

        allReports(id, sr, arr);
    }

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    function allReports(id, t, arr) {
        $.ajax({
            url: '/description/summary_report',
            dataType: 'JSON',
            type: 'POST',
            data: {jp: id, arr: arr, _token: token},
            success: function (data) {
                $('#main-content').attr('data-id', id);
                $('#main-content > div').hide();

                var html = '';
                html += '<h2> <img src="/assets/css/lib/heyling/right-back-arrow.png" alt=""> <a>' + data.jp.fields.position + '</a> <div><a class="asr active">Summary Report</a><a class="asbr">Skill Breakdown Report</a></div></h2>';
                html += '<div class="dotted-separate" style="padding: 10px 0;"></div>';
                html += '<div class="summary-filters">';
                html += '    <ul>';
                html += '        <li class="filter-primary-skills">Primary Skills:</li>';
                html += '        <li class="filter-ps-input">';
                if(data.primary_skills != '') {
                    html += '            <div>' + data.primary_skills + '</div>';
                }
                else {
                    html += '            <div>All</div>';
                }
                var array_skill = ['All'];
                html += '    <p>';
                for (var i2 in data.app_2) {
                    if (typeof(data.app_2[i2].data) != "undefined" && data.app_2[i2].data !== null) {
                        if (typeof(data.app_2[i2].data.special_data.ps) != "undefined" && data.app_2[i2].data.special_data.ps !== null) {
                            for (var i3 in data.app_2[i2].data.special_data.ps) {
                                if (typeof(data.app_2[i2].data.special_data.ps[i3]) != "undefined" && data.app_2[i2].data.special_data.ps[i3] !== null) {
                                    array_skill.push(data.app_2[i2].data.special_data.ps[i3]);
                                }
                            }
                        }
                    }
                }
                var unique = array_skill.filter(onlyUnique);
                for (var i4 in unique) {
                    html += '<a>' + unique[i4] + '</a>';
                }
                html += '    </p>';
                html += '        </li>';
                html += '        <li class="filter-summary-skills">Skills:</li>';
                html += '        <li class="filter-summary-skills-input">';
                if(data.all_skills != '') {
                    html += '            <div>' + data.all_skills + '</div>';
                }
                else {
                    html += '            <div>All</div>';
                }
                html += '            <p>';
                var array_skill2 = ['All'];
                for (var i5 in data.app_2) {
                    if (typeof (data.app_2[i5].data) != "undefined" && data.app_2[i5].data != null) {
                        if (typeof (data.app_2[i5].data.special_data.psx) != "undefined" && data.app_2[i5].data.special_data.psx != null) {
                            for (var i6 in data.app_2[i5].data.special_data.psx) {
                                array_skill2.push(data.app_2[i5].data.special_data.psx[i6].skill.trim());
                            }
                        }
                    }
                }
                var unique2 = array_skill2.filter(onlyUnique);
                for (var i7 in unique2) {
                    html += '<a>' + unique2[i7] + '</a>';
                }
                html += '            </p>';
                html += '        </li>';
                html += '        <li class="filter-summary-search"><input type="text" placeholder="Search" value="';
                if(data.txt_search != '') {
                    html += data.txt_search;
                }
                html += '"><a><img src="/assets/css/lib/heyling/search.png" alt=""></a></li>';
                html += '        <li class="filter-hidden">Hidden&nbsp;Files</li>';
                html += '        <li class="filter-hidden-input">';
                html += '            <div>Hide</div>';
                html += '            <p>';
                html += '            <a>All</a>';
                html += '            <a>Hide</a>';
                html += '            <a>Show</a>';
                html += '            </p>';
                html += '        </li>';
                html += '    </ul>';
                html += '</div>';

                html += '<div class="div-summary-report">';
                html += '<table cellpadding="0" cellspacing="0">';
                html += '    <tr>';
                html += '        <th>Applicant</th>';
                html += '        <th>Applied</th>';
                html += '        <th style="width:80px;">Matching<br>Score</th>';
                html += '        <th>Total<br>Experience</th>';
                html += '        <th>Current<br>company</th>';
                html += '        <th>Job change frequency</th>';
                html += '        <th>Highest<br>degree</th>';
                html += '        <th width="39"></th>';
                html += '        <th width="39"></th>';
                html += '        <th width="39"></th>';
                html += '    </tr>';

                var allApp = 0;
                for (var i in data.app) {
                    allApp++;

                    html += '    <tr data-id="' + data.app[i]._id.$id + '" class="td';
                    if (allApp % 2 == 0) {
                        html += ' grey-td';
                    }
                    if (typeof(data.app[i].hide) != "undefined" && data.app[i].hide !== null && data.app[i].hide == true) {
                        html += ' class-hide-row';
                    }
                    else {
                        html += ' class-show-row';
                    }
                    html += '"';
                    html += '>';
                    html += '        <td data-id="' + data.app[i]._id.$id + '" class="personal-page-one">';
                    html += '           <strong>';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.result_data.personal) != "undefined" && data.app[i].data.result_data.personal !== null) {
                            if (typeof(data.app[i].data.result_data.personal.names["0"]) != "undefined" && data.app[i].data.result_data.personal.names["0"] !== null) {
                                html += data.app[i].data.result_data.personal.names["0"].value;
                            }
                        }
                    }
                    html += '           </strong><br>';
                    html += '        <i>Email: ';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.result_data.personal) != "undefined" && data.app[i].data.result_data.personal !== null) {
                            if (typeof(data.app[i].data.result_data.personal.emails["0"]) != "undefined" && data.app[i].data.result_data.personal.emails["0"] !== null) {
                                html += data.app[i].data.result_data.personal.emails["0"].value;
                            }
                        }
                    }
                    html += '        </i></td>';
                    if ((data.app[i].date.$date.$numberLong + 86400) > (Date.now() / 1000)) {
                        html += '        <td><small> Today </small></td>';
                    }
                    else {
                        html += '<td><small>';
                        html += ParseData(parseInt(data.app[i].date.$date.$numberLong));
                        html += '</small></td>';
                    }
                    html += '        <td style="position:relative;">';
                    html += '        <div class="matching-score1">';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.match_data) != "undefined" && data.app[i].data.match_data !== null) {
                            if (typeof(data.app[i].data.match_data.matching_score) != "undefined" && data.app[i].data.match_data.matching_score !== null) {
                                html += parseFloat(data.app[i].data.match_data.matching_score).toFixed(1) + '%';
                            }
                            else {
                                html += '-';
                            }
                        }
                        else {
                            html += '-';
                        }
                    }
                    else {
                        html += '-';
                    }
                    html += '        </div>';
                    html += '        <div class="matching-score2">';
                    if (typeof(data.app[i].approved_date) != "undefined" && data.app[i].approved_date !== null) {
                        html += 'Final';
                    }
                    else {
                        html += 'Review';
                    }
                    html += '        </div>';
                    html += '</td>';
                    html += '<td>';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.special_data.tx) != "undefined" && data.app[i].data.special_data.tx !== null) {
                            html += jsDate(data.app[i].data.special_data.tx, 'sm');
                        }
                    }
                    html += '</td>';
                    html += '<td>';
                    var arr_company = '';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.result_data.experience) != "undefined" && data.app[i].data.result_data.experience !== null) {
                            if (typeof(data.app[i].data.result_data.experience.jobs) != "undefined" && data.app[i].data.result_data.experience.jobs !== null) {
                                for (var ej in data.app[i].data.result_data.experience.jobs) {
                                    if (typeof(data.app[i].data.result_data.experience.jobs[ej].date_range.current) != "undefined" && data.app[i].data.result_data.experience.jobs[ej].date_range.current !== null) {
                                        if (data.app[i].data.result_data.experience.jobs[ej].date_range.current == true) {
                                            if (typeof(data.app[i].data.result_data.experience.jobs[ej].company.value) != "undefined" && data.app[i].data.result_data.experience.jobs[ej].company.value !== null) {
                                                if (data.app[i].data.result_data.experience.jobs[ej].company.value != '' || data.app[i].data.result_data.experience.jobs[ej].company.value != ' ') {
                                                    arr_company += data.app[i].data.result_data.experience.jobs[ej].company.value + ', ';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var comma = rtrim(arr_company);
                    html += rtrim(comma);
                    html += '</td>';
                    html += '        <td>';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data !== null) {
                        if (typeof(data.app[i].data.special_data.jcf) != "undefined" && data.app[i].data.special_data.jcf !== null) {
                            html += jsDate(data.app[i].data.special_data.jcf, 'sm');
                        }
                    }
                    html += '</td>';
                    html += '<td>';
                    if (typeof(data.app[i].data) != "undefined" && data.app[i].data != null) {
                        if (typeof(data.app[i].data.special_data.highest_degree) != "undefined" && data.app[i].data.special_data.highest_degree != null) {
                            if (typeof(data.app[i].data.special_data.highest_degree.value) != "undefined" && data.app[i].data.special_data.highest_degree.value != null) {
                                html += data.app[i].data.special_data.highest_degree.value + ' ';
                            }
                        }
                    }
                    html += '</td>';
                    html += '<td>';
                    if (data.app[i].file_type == 'application/pdf') {
                        html += '<img src="/assets/css/lib/heyling/sm-pdf.png" alt="">';
                    }
                    else {
                        html += '<img src="/assets/css/lib/heyling/sm-word.png" alt="">';
                    }
                    html += '</td>';
                    html += '<td><a class="personal-page-one" data-id="' + data.app[i]._id.$id + '" href="javascript:void(0)"><img src="/assets/css/lib/heyling/tbl-user.png" alt=""></a></td>';
                    html += '<td>';
                    if (typeof(data.app[i].hide) != "undefined" && data.app[i].hide !== null && data.app[i].hide == true) {
                        html += '<a class="show-row" href="javascript:void(0)"><img src="/assets/css/lib/heyling/tbl-show.png" alt=""></a>';
                    }
                    else {
                        html += '<a class="hide-row" href="javascript:void(0)"><img src="/assets/css/lib/heyling/tbl-hide.png" alt=""></a>';
                    }
                    html += '</td>';
                    html += '</tr>';
                }

                html += '</table>';
                html += '</div>';

                html += '<div class="div-breakdown-report">';
                html += '<table cellpadding="0" cellspacing="0">';
                html += '    <tr>';
                html += '        <th>Applicant</th>';
                html += '        <th>Matching<br>Score</th>';
                html += '        <th>Total<br>Experience</th>';
                html += '        <th>Primary<br>Experience</th>';
                html += '        <td class="empty-rowspan" rowspan="' + (+allApp + 2) + '"></td>';
                html += '        <td rowspan="' + (+allApp + 2) + '">';
                html += '          <div>';
                html += '            <table cellpadding="0" cellspacing="0" class="new_custom_tbl">';
                html += '<tr>';
                var skills_num = 0;
                var skill_name = [];
                for (var s in data.jp.fields.skills) {
                    skill_name.push(data.jp.fields.skills[s].skill);
                    skills_num++;
                }
                for (var $j = 0; $j < skill_name.length; $j++) {
                    html += '<th width="' + (100 / skill_name.length) + '%">' + skill_name[$j] + '</th>';
                }
                html += '</tr>';
                for (var z in data.app) {
                    html += '<tr data-id="' + data.app[z]._id.$id + '" class="';
                    if (typeof(data.app[z].hide) != "undefined" && data.app[z].hide !== null && data.app[z].hide == true) {
                        html += 'class-hide-row';
                    }
                    else {
                        html += 'class-show-row';
                    }
                    html += '">';
                    for (var $k = 0; $k < skill_name.length; $k++) {
                        html += '<td>';
                        if (typeof(data.app[z].data) != "undefined" && data.app[z].data !== null) {
                            if (typeof(data.app[z].data.special_data.psx) != "undefined" && data.app[z].data.special_data.psx !== null) {
                                if (typeof(data.app[z].data.special_data.psx["0"]) != "undefined" && data.app[z].data.special_data.psx["0"] !== null) {
                                    for (var prop in data.app[z].data.special_data.psx) {
                                        if (data.app[z].data.special_data.psx[prop].skill.trim().toUpperCase() == skill_name[$k].trim().toUpperCase()) {
                                            html += jsDate(data.app[z].data.special_data.psx[prop].years, 'sm');
                                        }
                                    }
                                }
                            }
                        }
                        html += '</td>';
                    }
                    html += '</tr>';
                }
                html += '            </table>';
                html += '          </div>';
                html += '        </td>';
                html += '    </tr>';
                var colorful_4 = '';
                var color = 0;
                for (var j in data.app) {
                    if (color % 2) {
                        colorful_4 = '';
                    }
                    else {
                        colorful_4 = 'colorful_4';
                    }
                    html += '    <tr class="personal-page';
                    if (typeof(data.app[j].hide) != "undefined" && data.app[j].hide !== null && data.app[j].hide == true) {
                        html += ' class-hide-row';
                    }
                    else {
                        html += ' class-show-row';
                    }
                    html += '" data-id="' + data.app[j]._id.$id + '">';
                    html += '        <td>';
                    html += '           <strong>';
                    if (typeof(data.app[j].data) != "undefined" && data.app[j].data !== null) {
                        if (typeof(data.app[j].data.result_data.personal) != "undefined" && data.app[j].data.result_data.personal !== null) {
                            if (typeof(data.app[j].data.result_data.personal.names["0"]) != "undefined" && data.app[j].data.result_data.personal.names["0"] !== null) {
                                html += data.app[j].data.result_data.personal.names["0"].value;
                            }
                        }
                    }
                    html += '           </strong><br>';
                    if (typeof(data.app[j].data) != "undefined" && data.app[j].data !== null) {
                        if (typeof(data.app[j].data.result_data.personal) != "undefined" && data.app[j].data.result_data.personal !== null) {
                            if (typeof(data.app[j].data.result_data.personal.emails["0"]) != "undefined" && data.app[j].data.result_data.personal.emails["0"] !== null) {
                                html += data.app[j].data.result_data.personal.emails["0"].value;
                            }
                        }
                    }
                    html += '        </td>';
                    html += '        <td class="' + colorful_4 + '">';
                    if (typeof(data.app[j].data) != "undefined" && data.app[j].data !== null) {
                        if (typeof(data.app[j].data.match_data) != "undefined" && data.app[j].data.match_data !== null) {
                            if (typeof(data.app[j].data.match_data.matching_score) != "undefined" && data.app[j].data.match_data.matching_score !== null) {
                                html += data.app[j].data.match_data.matching_score + '%';
                            }
                        }
                    }
                    else {
                        html += '-';
                    }
                    html += '</td>';
                    html += '        <td class="' + colorful_4 + '">';
                    if (typeof(data.app[j].data) != "undefined" && data.app[j].data !== null) {
                        if (typeof(data.app[j].data.special_data.tx) != "undefined" && data.app[j].data.special_data.tx !== null) {
                            html += jsDate(data.app[j].data.special_data.tx, 'sm');
                        }
                    }
                    html += '</td>';
                    html += '        <td class="' + colorful_4 + '">';
                    if (typeof(data.app[j].data) != "undefined" && data.app[j].data !== null) {
                        if (typeof(data.app[j].data.special_data.ps) != "undefined" && data.app[j].data.special_data.ps !== null) {
                            html += data.app[j].data.special_data.ps["0"];
                        }
                    }
                    html += '</td>';
                    html += '    </tr>';
                    color++;
                }
                html += '    <tr>';
                html += '        <td colspan="4"></td>';
                html += '    </tr>';
                html += '</table>';
                html += '</div>';

                $('#main-content').append('<div class="summary_report">' + html + '</div>');

                if (t === 'Breakdown Report') {
                    setTimeout(function () {
                        $('.asbr').click();
                    }, 100);
                }
            }
        });
    }

    $(document).on('click', '.hide-row', function () {
        var $this = $(this);
        var id = $this.closest('tr').attr('data-id');
        $.ajax({
            url: '/heyling/ajax/hide_row',
            data: {id: id},
            dataType: 'JSON',
            type: 'POST',
            success: function () {
                $this.closest('tr').addClass('class-hide-row').removeClass('class-show-row');
                if ($('.filter-hidden-input > div').text() !== 'All') {
                    $this.closest('tr').fadeOut();
                }
                $this.removeClass('hide-row').addClass('show-row');
                $this.find('img').attr('src', '/assets/css/lib/heyling/tbl-show.png');
                $('.new_custom_tbl tr[data-id="' + id + '"], tr.personal-page[data-id="' + id + '"]').addClass('class-hide-row').removeClass('class-show-row');
            }
        });
    });

    $(document).on('click', '.show-row', function () {
        var $this = $(this);
        var id = $this.closest('tr').attr('data-id');
        $.ajax({
            url: '/heyling/ajax/show_row',
            data: {id: id},
            dataType: 'JSON',
            type: 'POST',
            success: function () {
                $this.closest('tr').removeClass('class-hide-row').addClass('class-show-row');
                if ($('.filter-hidden-input > div').text() !== 'All') {
                    $this.closest('tr').fadeOut();
                }
                $this.addClass('hide-row').removeClass('show-row');
                $this.find('img').attr('src', '/assets/css/lib/heyling/tbl-hide.png');
                $('.new_custom_tbl tr[data-id="' + id + '"], tr.personal-page[data-id="' + id + '"]').removeClass('class-hide-row').addClass('class-show-row');
            }
        });
    });

    $(document).on('click', 'a[data-jd]', function () {
        var id = $(this).attr('data-jd');
        allReports(id, 'Summary Report');
    });

    function removeOpenedApp() {
        $('.opened-jd').css('bottom', '-200px');
        setTimeout(function () {
            $('.opened-jd').remove();
        }, 300);
    }

    $(document).on('click', '.opened-jd a', function () {
        removeOpenedApp();
    });

    function jsDate(num, sm) {
        var years = parseInt(num);
        var months = num - years;
        months = months * 12;
        var m = Math.round(months);
        if (m == 12) {
            m = 0;
            years++;
        }
        var str = '';
        var a = ' years ';
        var b = ' months ';
        if (sm === 'sm') {
            a = 'y ';
            b = 'm ';
        }
        else if (sm === 'min') {
            a = '.';
            b = ' ';
        }
        if (years > 0) {
            str += years + a;
        }
        if (m > 0) {
            str += m + b;
        }
        return str;
    }

    $(document).on('click', '.personal_page > h1 > img', function () {
        var id = null;
        if (options.backPathId) {
            id = options.backPathId;
        }
        if (id == null) {
            window.location = '/heyling';
        }
        else {
            if (options.backPath == 'summary-report') {
                allReports(id, $(this).val());
                $('.personal_page').remove();
            }
            else {
                allReports(id, $(this).val());
                $('.personal_page').remove();
            }
        }
    });

    $(document).on('click', '.asr:not(.active)', function () {
        $(this).toggleClass('active');
        $('.asbr').removeClass('active');
        $('.div-summary-report').show();
        $('.div-breakdown-report').hide();
        $('.filter-hidden, .filter-hidden-input').css('opacity', 1);
    });

    $(document).on('click', '.asbr:not(.active)', function () {
        $(this).toggleClass('active');
        $('.asr').removeClass('active');
        $('.div-breakdown-report').show();
        $('.div-summary-report').hide();
        var winWidth = $(window).width();
        var v924 = 871;
        if (options.leftPanel) {
            v924 = v924 - 235;
        }
        $('.div-breakdown-report > table > tbody > tr > td:nth-child(6) > div').css('width', (winWidth - v924) + 'px');
        $('.filter-hidden, .filter-hidden-input').css('opacity', 0);
        removeOpenedApp();
    });

    function personalPage(id) {
        $.ajax({
            url: '/heyling/ajax/personal_page',
            dataType: 'JSON',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                $('.center-loader').remove();
                $('.summary_report').hide();

                var html = '';

                html += '<h1> <img src="/assets/css/lib/heyling/right-back-arrow.png" alt=""> Candidate Detailed View</h1>';
                html += '<div class="dotted-separate" style="padding: 15px 0;"></div>';

                html += '<table class="with-white-td" cellspacing="0" cellpadding="0">';
                html += '<tr>';
                html += '<td>';
                if (data.data.file_type == 'application/pdf') {
                    html += '<img src="/assets/css/lib/heyling/pdf.png" alt="">';
                }
                else {
                    html += '<img src="/assets/css/lib/heyling/word.png" alt="">';
                }
                html += '</td>';
                html += '<td>';
                if (typeof(data.data.data.result_data.personal) != "undefined" && data.data.data.result_data.personal !== null) {
                    if (typeof(data.data.data.result_data.personal.names["0"]) != "undefined" && data.data.data.result_data.personal.names["0"] !== null) {
                        html += '<b>' + data.data.data.result_data.personal.names["0"].value + '</b><br>';
                    }
                }
                if (typeof(data.data.data.result_data.personal) != "undefined" && data.data.data.result_data.personal !== null) {
                    if (typeof(data.data.data.result_data.personal.emails["0"]) != "undefined" && data.data.data.result_data.personal.emails["0"] !== null) {
                        html += '<span>' + data.data.data.result_data.personal.emails["0"].value + '</span>';
                    }
                }
                html += '</td>';
                html += '<td>';
                if (typeof(data.data.data.result_data.personal) != "undefined" && data.data.data.result_data.personal !== null) {
                    if (typeof(data.data.data.result_data.personal.locations["0"]) != "undefined" && data.data.data.result_data.personal.locations["0"] !== null) {
                        html += data.data.data.result_data.personal.locations["0"].value;
                    }
                }
                html += '</td>';
                html += '<td>';
                if (typeof(data.data.date) != "undefined" && data.data.date !== null) {
                    html += 'Applied: ' + convertTimestamp(data.data.date.sec);
                }
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                if (typeof(data.data.data.match_data.matching_score) != "undefined" && data.data.data.match_data.matching_score !== null) {
                    html += '<span class="percent-white-box">' + parseFloat(data.data.data.match_data.matching_score).toFixed(1) + '%</span>';
                }
                html += '</td>';
                html += '<td>';
                html += 'Matching Score';
                html += '</td>';
                html += '<td colspan="2">';
                if (typeof(data.data.data.approved) != "undefined" && data.data.data.approved !== null) {
                    if (data.data.data.approved == 1) {
                        if (typeof(data.data.approved_date) != "undefined" && data.data.approved_date !== null) {
                            html += 'Status is final. Candidate update the profile: ' + convertTimestamp(data.data.approved_date);
                        }
                    }
                }
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                html += '<span class="white-total">';
                if (typeof(data.data.data.special_data.tx) != "undefined" && data.data.data.special_data.tx !== null) {
                    html += jsDate(data.data.data.special_data.tx, 'sm');
                }
                html += '</span>';
                html += '</td>';
                html += '<td colspan="3">';
                html += 'Total Experience';
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                html += '<span class="grey-total">Primary<br>Skills</span>';
                html += '</td>';
                html += '<td colspan="3" class="no-comma">';
                if (typeof(data.data.data.special_data.psx) != "undefined" && data.data.data.special_data.psx !== null) {
                    if (typeof(data.data.data.special_data.psx["0"]) != "undefined" && data.data.data.special_data.psx["0"] !== null) {
                        for (var prop in data.data.data.special_data.psx) {
                            if (data.data.data.special_data.psx[prop].years > 0) {
                                html += data.data.data.special_data.psx[prop].skill.trim() + '<em>,</em> ';
                            }
                        }
                    }
                }
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                html += '<span class="grey-total">Primary<br>Job<br>Functions</span>';
                html += '</td>';
                html += '<td colspan="3">';
                if (typeof(data.data.data.special_data.pjf) != "undefined" && data.data.data.special_data.pjf !== null) {
                    if (typeof(data.data.data.special_data.pjf["0"]) != "undefined" && data.data.data.special_data.pjf["0"] !== null) {
                        for (var prop in data.data.data.special_data.pjf) {
                            html += data.data.data.special_data.pjf[prop] + '<br>';
                        }
                    }
                }
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                html += '<span class="white-total">Current<br>Company</span>';
                html += '</td>';
                html += '<td colspan="3">';
                var cc = '';
                if (typeof(data.data.data.result_data.experience) != "undefined" && data.data.data.result_data.experience !== null) {
                    if (typeof(data.data.data.result_data.experience.jobs) != "undefined" && data.data.data.result_data.experience.jobs !== null) {
                        for (var ej in data.data.data.result_data.experience.jobs) {
                            if (typeof(data.data.data.result_data.experience.jobs[ej].date_range.current) != "undefined" && data.data.data.result_data.experience.jobs[ej].date_range.current !== null) {
                                if (data.data.data.result_data.experience.jobs[ej].date_range.current == true) {
                                    if (typeof(data.data.data.result_data.experience.jobs[ej].company.value) != "undefined" && data.data.data.result_data.experience.jobs[ej].company.value !== null) {
                                        cc = data.data.data.result_data.experience.jobs[ej].company.value;
                                    }
                                }
                            }
                        }
                    }
                }
                html += cc;
                html += '</td>';
                html += '</tr>';
                html += '<tr>';
                html += '<td>';
                html += '<span class="grey-total">Highest<br>Degree</span>';
                html += '</td>';
                html += '<td colspan="3">';
                if (typeof(data.data.data) != "undefined" && data.data.data != null) {
                    if (typeof(data.data.data.special_data) != "undefined" && data.data.data.special_data != null) {
                        if (typeof(data.data.data.special_data.highest_degree) != "undefined" && data.data.data.special_data.highest_degree != null) {
                            if (typeof(data.data.data.special_data.highest_degree.value) != "undefined" && data.data.data.special_data.highest_degree.value != null) {
                                html += data.data.data.special_data.highest_degree.value + ' ';
                            }
                        }
                    }
                }
                html += '</td>';
                html += '</tr>';

                html += '<tr class="crt-id">';
                html += '<td colspan="4" id="hide-js">';
                var obj = [];
                var crt = true;
                if (typeof(data.data.data.result_data.experience) != "undefined" && data.data.data.result_data.experience !== null) {
                    if (typeof(data.data.data.result_data.experience.jobs) != "undefined" && data.data.data.result_data.experience.jobs !== null) {
                        for (var pp in data.data.data.result_data.experience.jobs) {
                            if (typeof(data.data.data.result_data.experience.jobs[pp].company) != "undefined" &&
                                data.data.data.result_data.experience.jobs[pp].company !== null &&
                                typeof(data.data.data.result_data.experience.jobs[pp].date_range) != "undefined" &&
                                data.data.data.result_data.experience.jobs[pp].date_range !== null
                            ) {
                                if (data.data.data.result_data.experience.jobs[pp].company.value &&
                                    data.data.data.result_data.experience.jobs[pp].date_range.duration &&
                                    data.data.data.result_data.experience.jobs[pp].date_range.duration > 0
                                ) {
                                    obj.push({
                                        label: data.data.data.result_data.experience.jobs[pp].company.value,
                                        y: data.data.data.result_data.experience.jobs[pp].date_range.duration,
                                        name: jsDate(data.data.data.result_data.experience.jobs[pp].date_range.duration)
                                    });
                                    crt = false;
                                }
                            }
                        }
                    }
                }
                html += '<div id="chartContainer" style="height: 180px; width: 100%;"></div>';
                html += '<div class="hide-js"></div><div class="hide-js2"></div>';
                html += '</td>';
                html += '</tr>';

                html += '<tr>';
                html += '<td>';
                html += '<span class="white-total">';
                if (typeof(data.data.data.special_data.jcf) != "undefined" && data.data.data.special_data.jcf !== null) {
                    html += '<b>' + jsDate(data.data.data.special_data.jcf, 'min') + '</b>';
                }
                html += '</span>';
                html += '</td>';
                html += '<td colspan="3">';

                if (typeof(data.data.data.special_data.jcf) != "undefined" && data.data.data.special_data.jcf !== null) {
                    html += 'Based on Job Changing Frequency, the next job’s expected duration is ' + jsDate(data.data.data.special_data.jcf, 'full');
                }

                html += '</td>';
                html += '</tr>';

                html += '<tr>';
                html += '<td>';
                html += '<span class="grey-total">Education</span>';
                html += '</td>';
                html += '<td colspan="3">';
                if (typeof(data.data.data.result_data.education) != "undefined" && data.data.data.result_data.education !== null) {
                    if (typeof(data.data.data.result_data.education.institutions) != "undefined" && data.data.data.result_data.education.institutions !== null) {
                        for (var prop in data.data.data.result_data.education.institutions) {
                            if (typeof(data.data.data.result_data.education.institutions[prop].name) != "undefined" && data.data.data.result_data.education.institutions[prop].name !== null) {
                                if (typeof(data.data.data.result_data.education.institutions[prop].name.value) != "undefined" && data.data.data.result_data.education.institutions[prop].name.value !== null) {
                                    html += data.data.data.result_data.education.institutions[prop].name.value + '<br>';
                                }
                            }
                        }
                    }
                }
                html += '</td>';
                html += '</tr>';

                html += '<tr>';
                html += '<td>';
                html += '<span class="white-total">Overall<br>Skills</span>';
                html += '</td>';
                html += '<td colspan="3" class="no-comma">';
                if (typeof(data.data.data.special_data.os) != "undefined" && data.data.data.special_data.os !== null) {
                    for (var os in data.data.data.special_data.os) {
                        html += data.data.data.special_data.os[os].trim() + '<em>,</em> ';
                    }
                }
                html += '</td>';
                html += '</tr>';
                html += '</table>';

                $('#main-content').append('<div class="personal_page">' + html + '</div>');

                setTimeout(function () {
                    var options2 = {
                        title: {
                            text: ""
                        },
                        data: [
                            {
                                type: "column",
                                color: "#5b9bd5",
                                toolTipContent: "{label}: {name}",
                                dataPoints: obj
                            }
                        ]
                    };
                    $("#chartContainer").CanvasJSChart(options2);
                    if (crt) {
                        $('.crt-id').hide();
                    }
                }, 1500);
            }
        });
    }

    function convertTimestamp(timestamp) {
        var d = new Date(timestamp * 1000),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;
        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh == 0) {
            h = 12;
        }
        time = dd + '.' + mm + '.' + yyyy + ' ' + h + ':' + min + ' ' + ampm;
        return time;
    }

    $(document).on('click', '.personal-page', function () {
        options.backPath = 'summary-report';
        options.backPathId = window.location.hash.split('/').pop();
        personalPage($(this).attr('data-id'));
    });

    $(document).on('click', '.personal-page-one', function () {
        options.backPath = 'skill-breakdown-report';
        options.backPathId = window.location.hash.split('/').pop();
        personalPage($(this).attr('data-id'));
    });

    function showAppList(id, txt, skill_text, str) {
        $.ajax({
            url: '/home/get_list',
            dataType: 'JSON',
            data: {id: id, skill_text: skill_text, str: str, _token: token},
            type: 'POST',
            success: function (data) {
                $('.active_job_posting, .closed_job_postings, .for-white-link').remove();
                if (txt.length > 27) {
                    txt = txt.slice(0, 25) + '..';
                }
                $('.filter-projects-input > div').text(txt);
                $('.filter-projects-input > div').attr('data-id', id);
                $('.filter-projects-input > p, .filter-skills-input > p').find('a[data-id="0"]').remove();
                $('.filter-projects-input > p, .filter-skills-input > p').prepend('<a data-id="0">All</a>');
                if (skill_text.length === 0) {
                    $('.filter-skills-input > div').text('No skills');
                }
                else {
                    $('.filter-skills-input > div').text(skill_text);
                }
                $('.filter-search input').val(str);
                $('#left-dashboard').addClass('active');
                $('.team_name').hide();
                if (txt === 'All') {
                    $('#left-dashboard').removeClass('active');
                    $('#left-teams, .team_name').show();
                    $('.active_job_posting, .closed_job_postings, .for-white-link').remove();
                }
                else {
                    $('.separate').after('<a class="active_job_posting"><span></span>' + txt + '</a> <a class="closed_job_postings" data-id="' + id + '">Job Postings <img src="/assets/css/lib/heyling/plus.png" alt=""></a>');
                }
                var html = '';
                var i = 0;
                for (var p in data.posts) {
                    html += '<div';
                    if (i % 2) {
                        html += ' class="ml"';
                    }
                    html += '>';
                    html += '<div class="top-section">';
                    html += '<strong><a data-id="' + data.posts[p]._id.$id + '"> ';
                    if (data.posts[p].position.length > 50) {
                        html += data.posts[p].position.slice(0, 48) + '..';
                    }
                    else {
                        html += data.posts[p].position;
                    }
                    html += ' </a></strong>';
                    html += '<date> ' + data.posts[p].date + ' </date>';
                    html += '<span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>';
                    html += '<span> Resumes: ';
                    if (data.resumes[data.posts[p]._id.$id]) {
                        html += '<a data-jd="' + data.posts[p]._id.$id + '" data-pi="' + data.posts[p].project_id + '">';
                        html += data.resumes[data.posts[p]._id.$id].length;
                        html += '</a> ';
                    }
                    else {
                        html += 0;
                    }
                    html += '</span>';
                    html += '<div>';
                    html += 'Group: <b>';
                    if (data.project_name[data.posts[p].project_id].length > 25) {
                        html += data.project_name[data.posts[p].project_id].slice(0, 23) + '..';
                    }
                    else {
                        html += data.project_name[data.posts[p].project_id];
                    }
                    html += '</b>';
                    html += '<mail>' + data.posts[p].email + '</mail>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="bottom-section">';
                    html += 'Skills: ';
                    if (data.posts[p].skills && data.posts[p].skills.length > 0) {
                        var c = 0, n = 0;
                        var skills_array = [];
                        for (var z in data.posts[p].skills) {
                            if (c < 3) {
                                skills_array.push(data.posts[p].skills[z]);
                                n++;
                            }
                            c++;
                        }
                        if ((c - n) > 0) {
                            skills_array.push('<em>+' + (c - n) + '</em>');
                        }
                        skills_array.reverse();
                        for (var r in skills_array) {
                            html += skills_array[r];
                        }
                    }
                    html += '</div>';
                    html += '</div>';
                    i++;
                }

                $('.main-list').html(html);
                setTimeout(function () {
                    $('.closed_job_postings').click();
                }, 100);
            }
        });
    }

    function updateTemplate() {
        $.ajax({
            url: '/description/load_select_boxes',
            async: false,
            dataType: 'JSON',
            type: 'POST',
            data: {_token: token},
            success: function (data) {
                var html = '';
                html += '    <div class="main-title">';
                html += '        <h2>Dashboard</h2>';
                html += '        <h6>Check out what’s hapenning!</h6>';
                html += '    </div>';
                html += '    <div class="row">';
                html += '        <div class="col welcome-box">';
                html += '            <div>';
                html += '                <h3>Welcome to TextSlice Recruitment</h3>';
                html += '                Create a new <a class="add-new-group">Group</a> or a new <a class="add-new-job-posting">Job Posting</a> to start.';
                html += '                You always can edit or remove the groups and postings before you make them public and receive applications.<br>';
                html += '                The dashboard automatically reflects the latest candidate activities, up to date reports and insights.';
                html += '            </div>';
                html += '        </div>';
                html += '        <div class="col jobs-box">';
                var job_s = 's';
                if (data.posted_this_week_jobs == 1) {
                    job_s = ''
                }
                html += '            <h3><b>' + data.posted_this_week_jobs + '</b> Posting' + job_s + '</h3>';
                html += '            <span>created this week</span>';
                html += '        </div>';
                html += '        <div class="col applicants-box">';
                var app_s = 's';
                if (data.posted_this_week_applicants == 1) {
                    app_s = ''
                }
                html += '            <h3><b>' + data.posted_this_week_applicants + '</b> Applicant' + app_s + '</h3>';
                html += '            <span>applied this week</span>';
                html += '        </div>';
                html += '    </div>';
                html += '    <div class="dotted-separate"></div>';
                html += '    <div class="main"><div class="main-filters">';
                html += '    <ul>';
                html += '       <li class="filter-projects">Groups:</li>';
                html += '       <li class="filter-projects-input"><div>All</div>';
                html += '        <p>';
                html += '<a data-id="0">All</a>';
                for (var i in data.projects) {
                    html += '<a data-id="' + data.projects[i].id + '">' + data.projects[i].name + '</a>';
                }
                html += '       </p></li>';
                html += '       <li class="filter-skills">Skills:</li>';
                if (data.skill_status === 0) {
                    html += '       <li class="filter-skills-input"><div>No skills</div><p>';
                }
                else {
                    html += '       <li class="filter-skills-input"><div>All</div><p>';
                    for (var s in data.skills) {
                        html += '<a>' + data.skills[s] + '</a>';
                    }
                }
                html += '       </p></li>';
                html += '       <li class="filter-search"><input type="text" placeholder="Search"><a><img src="/assets/css/lib/heyling/search.png" alt=""></a></li>';
                html += '    </ul>';
                html += '    </div><div class="main-list"></div>';
                html += '    </div>';

                $('#main-content').html(html);
            }
        });
    }

    function removeOpenJP() {
        $('.jp_header, .jp, .save_panel').remove();
    }

    $(document).on({
        mouseenter: function () {
            $(this).append($("<a></a>"));
        },
        mouseleave: function () {
            $(this).find("a:last").remove();
        }
    }, 'ul.with-scroll > li.team_name:not(.not_it)');

    $(document).on('click', 'li.team_name:not(.not_it)', function (e) {
        var $this = $(this);
        var id = $this.attr('id');
        var text_project = $this.text();
        if (e.target.tagName == 'A') {
            $this.addClass('not_it');
            $this.html('<input type="text" value="' + text_project.trim() + '"> <x></x>');
            $this.find('input').focus();
        }
        else {
            var text_skill = $('.filter-skills-input > div').text();
            removeOpenJP();
            updateTemplate();
            showAppList(id, text_project, text_skill, $('.filter-search input').val());
            $('#left-teams').addClass('left-teams-pointer');
            $('.groups_add_btn').remove();
        }
    });

    $(document).on('click', '.not_it x', function () {
        jobPosting($(this));
    });

    $(document).on('keyup', '.not_it input', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            jobPosting($(this).closest('li').find('x'));
        }
    });

    function jobPosting($this) {
        var id = $this.closest('li').attr('id');
        var txt = $this.closest('li').find('input').val();
        if (txt == '' || txt == ' ') {
            $this.closest('li').find('input').attr('placeholder', 'Insert Group Name');
        }
        else {
            $.ajax({
                url: '/description/update_job_posting',
                type: 'POST',
                data: {id: id, txt: txt, _token: token},
                success: function () {
                    $this.closest('li').removeClass('not_it');
                    $this.closest('li').text(txt);
                    if ($('li.jp_header_pn > b').length) {
                        $('li.jp_header_pn > b').text(txt);
                    }
                }
            });
        }

        return false;
    }

    $(document).on('click', '.closed_job_postings:not(.active)', function () {
        var $this = $(this);
        $this.addClass('active');
        $.ajax({
            url: '/description/get',
            dataType: 'JSON',
            type: 'POST',
            data: {id: $this.data('id'), _token: token},
            success: function (data) {
                var html = '<div class="for-white-link">';
                var links = 0;
                for (var p in data) {
                    html += '<a data-id="' + data[p]._id.$oid + '" class="white-link">' + data[p].fields.position + '</a>';
                    links++;
                }
                html += '</div>';
                $this.after(html);
                sizeCorrections();
                var str_10 = $('a.active_job_posting').text();
                if (str_10.length > 7) {
                    str_10 = str_10.slice(0, 6) + '..';
                }
                $('#left-side').append('<a class="remove_project">Remove "' + str_10.trim() + '" Group <img src="/assets/css/lib/heyling/trush-project.png" alt=""></a>');
            }
        });
    });

    $(document).on('click', '.white-link:not(.active)', function () {
        $('.white-link').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        $('#main-content').attr('data-jp', id);
        $.ajax({
            url: '/description/edit',
            dataType: 'JSON',
            type: 'POST',
            data: {id: id, _token: token},
            success: function (data) {
                JP(data);
            }
        });
    });

    $(document).on('click', '.edit_jp_header a', function () {
        $(this).closest('li').html('<input type="button" class="jp_header_save" value="Save"> <input type="button" class="jp_header_cancel" value="Cancel">');
        var jp_header_jpn = $('.jp_header .jp_header_jpn b');
        var jp_header_sl = $('.jp_header .jp_header_sl b');
        var jp_header_tx = $('.jp_header .jp_header_tx b');
        jp_header_jpn.html('<input type="text" value="' + jp_header_jpn.text() + '" data-cancel="' + jp_header_jpn.text() + '" class="jp_header_jpn">');
        jp_header_sl.html('<span data-cancel="' + jp_header_sl.text() + '" class="jp_header_sl">' + jp_header_sl.text() + '</span>');
        jp_header_tx.html('<input type="number" value="' + jp_header_tx.text() + '" data-cancel="' + jp_header_tx.text() + '" class="jp_header_tx">');
    });

    $(document).on('keydown', '#hidden_input', function (e) {
        $('.jp_header_tx').focus();
    });

    $(document).on('keydown', 'body', function (e) {
        var code = e.keyCode || e.which;
        if ($('.jp_header_save').length && $('.jp_header_cancel').length && !$('.jp_header_sl dd').length) {
            if (code == '13') {
                $('.jp_header_save').click();
            }
            else if (code == '27') {
                $('.jp_header_cancel').click();
            }
        }
        if ($('.jp_header_save').length && $('.jp_header_cancel').length) {
            var step = 1;
            if (code == '40') {
                if ($('.active_jms:nth-child(1)').length) {
                    $('.jp_header_sl dd a:nth-child(1)').removeClass('active_jms');
                    step = 2;
                }
                else if ($('.active_jms:nth-child(2)').length) {
                    $('.jp_header_sl dd a:nth-child(2)').removeClass('active_jms');
                    step = 3;
                }
                else if ($('.active_jms:nth-child(3)').length) {
                    $('.jp_header_sl dd a:nth-child(3)').removeClass('active_jms');
                    step = 1;
                }
                $('.jp_header_sl dd a:nth-child(' + step + ')').addClass('active_jms');
            }
            else if (code == '38') {
                if ($('.active_jms:nth-child(3)').length) {
                    $('.jp_header_sl dd a:nth-child(3)').removeClass('active_jms');
                    step = 2;
                }
                else if ($('.active_jms:nth-child(2)').length) {
                    $('.jp_header_sl dd a:nth-child(2)').removeClass('active_jms');
                    step = 1;
                }
                else if ($('.active_jms:nth-child(1)').length) {
                    $('.jp_header_sl dd a:nth-child(1)').removeClass('active_jms');
                    step = 3;
                }
                $('.jp_header_sl dd a:nth-child(' + step + ')').addClass('active_jms');
            }
            else if (code == '13' && $('.jp_header_sl dd').length) {
                $('.active_jms').click();
                $('.jp_header_tx').focus();
            }
        }
    });

    $(document).on('keydown', '.jp_header_jpn', function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') {
            $('.jp_header_sl').click();
        }
    });

    $(document).on('click', '.top-section strong a', function () {
        var id = $(this).attr('data-id');
        resultsPages(id);
    });

    $(document).on('click', '.summary_report > h2 > a', function () {
        var id = window.location.hash.split('/');
        resultsPages(id[2]);
    });

    function resultsPages(id) {
        $.ajax({
            url: '/description/edit',
            dataType: 'JSON',
            type: 'POST',
            data: {id: id, _token: token},
            success: function (data) {
                JP(data);
            }
        });
    }

    $(document).on('click', '.jp_header_cancel', function () {
        var jp_header_jpn = $('input.jp_header_jpn');
        var jp_header_sl = $('span.jp_header_sl');
        var jp_header_tx = $('input.jp_header_tx');
        var jp_header_jpn_b = $('.jp_header .jp_header_jpn b');
        var jp_header_sl_b = $('.jp_header .jp_header_sl b');
        var jp_header_tx_b = $('.jp_header .jp_header_tx b');
        jp_header_jpn_b.html(jp_header_jpn.attr('data-cancel'));
        jp_header_sl_b.html(jp_header_sl.attr('data-cancel'));
        jp_header_tx_b.html(jp_header_tx.attr('data-cancel'));
        $('.edit_jp_header').html('<a></a>');
    });

    $(document).on('click', '.jp_header_save', function () {
        var jp_header_jpn = $('input.jp_header_jpn');
        var jp_header_sl = $('span.jp_header_sl');
        var jp_header_tx = $('input.jp_header_tx');
        var jp_header_jpn_b = $('.jp_header .jp_header_jpn b');
        var jp_header_sl_b = $('.jp_header .jp_header_sl b');
        var jp_header_tx_b = $('.jp_header .jp_header_tx b');
        if (jp_header_jpn.val() == '') {
            jp_header_jpn.attr('placeholder', 'Please insert Group Name!');
        }
        else if (jp_header_sl.text() == '') {
            jp_header_sl.attr('placeholder', 'Please insert Seniority Level!');
        }
        else {
            $.ajax({
                url: '/description/update_hi',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    posting: jp_header_jpn.val(),
                    level: jp_header_sl.text(),
                    total_experience: jp_header_tx.val(),
                    _token: token
                },
                success: function (data) {
                    jp_header_jpn_b.html(data.position);
                    jp_header_sl_b.html(data.seniority);
                    jp_header_tx_b.html(data.total_experience);
                    $('.edit_jp_header').html('<a></a>');
                }
            });
        }
    });

    $(document).on('click', '.jp_header_sl span', function () {
        $('li.jp_header_sl dd').remove();
        $('li.jp_header_sl').append('<dd><a>Junior</a><a>Mid-level</a><a>Senior</a></dd>');
    });

    $(document).on('click', 'li.jp_header_sl dd a', function () {
        $('.jp_header_sl span').text($(this).text());
        $('.jp_header_tx').focus();
    });

    $(document).on('click', '.active_job_posting span', function () {
        $('.remove_project').remove();
        $('#left-dashboard').removeClass('active');
        $('#left-teams, .team_name').show();
        $('.active_job_posting, .closed_job_postings, .for-white-link').remove();
        $('.left-teams-pointer').removeClass('left-teams-pointer');
        $('ul.with-scroll').before('<a class="groups_add_btn">Groups</a>');
        showAppList(0, 'All', '', '');
    });

    $(document).on('click', '.left-teams-pointer', function () {
        window.location = '/heyling';
    });

    $(document).on('click', '.summary_report > h2 > img', function () {
        var id = window.location.hash.split('/').pop();
        if (options.backPath == 'job-posting') {
            resultsPages(id);
        }
        else {
            window.location = '/heyling';
        }
    });

    $(document).on('click', '.remove_project', function () {
        var html = '<div class="remove_project_note">';
        html += 'Please confirm. <br>';
        $.ajax({
            url: '/heyling/ajax/detect_applicants_count',
            dataType: 'JSON',
            type: 'POST',
            data: {id: $('.closed_job_postings').attr('data-id')},
            success: function (data) {
                var job_s = 's';
                if ($('.for-white-link a').length > 0 && data > 0) {
                    var app_s = 's';
                    if ($('.for-white-link a').length == 1) {
                        job_s = '';
                    }
                    if (data == 1) {
                        app_s = '';
                    }
                    html += '<b>' + $('.for-white-link a').length + '</b> job posting' + job_s + ' and <b>' + data + '</b> candidate information will be deleted';
                }
                else if ($('.for-white-link a').length > 0) {
                    if ($('.for-white-link a').length == 1) {
                        job_s = '';
                    }
                    html += '<b>' + $('.for-white-link a').length + '</b> job posting' + job_s + ' will be deleted.';
                }
                html += '<br> <a>Delete</a> <a>Cancel</a></div>';
                $('#left-side').append(html);
            }
        });
    });

    $(document).on('click', '.remove_project_note a', function () {
        if ($(this).text() === 'Cancel') {
            $('.remove_project_note').remove();
        }
        else {
            $.ajax({
                url: '/heyling/ajax/remove_project',
                dataType: 'JSON',
                type: 'POST',
                data: {id: $('.closed_job_postings').attr('data-id')},
                success: function () {
                    window.location = '/heyling';
                }
            });
        }
    });

    $(document).on('click', 'a, div, span', function () {
        if ($(this).text() !== 'Yes') {
            $('.remove_project_note').remove();
        }
    });

    $(document).on('click', '.qr-txt > img', function () {
        var $this = $(this);
        var id = $('#main-content').attr('data-jp');
        var txt = $this.closest('.qr-txt').text().trim();
        var f = $this.closest('div[class^="jp_form_"]').attr('class').substring(8);
        var str = 'job requirement';
        if (f == 5) {
            str = 'question';
            txt = txt.substring(10);
        }
        var res = confirm('Are you sure you want to delete the "' + txt.trim() + '" ' + str + '?');
        if (res) {
            $.ajax({
                url: '/heyling/ajax/remove_q',
                dataType: 'JSON',
                type: 'POST',
                data: {id: id, q: txt, f: f},
                success: function () {
                    $this.parent('div').remove();
                }
            });
        }
    });

    $(document).on('click', 'img[src="/assets/css/lib/heyling/sm-word.png"], img[src="/assets/css/lib/heyling/sm-pdf.png"]', function () {
        showFile($(this).closest('tr').attr('data-id'));
    });

    $(document).on('click', 'img[src="/assets/css/lib/heyling/word.png"], img[src="/assets/css/lib/heyling/dashboard.pngpdf.png"]', function () {
        var id = window.location.href.split('/');
        var end_id = id[id.length - 1];
        showFile(end_id);
    });

    function showFile(id) {
        $('body').append('<div class="word-pdf-view"><iframe src="https://docs.google.com/gview?url=https://textslice.com/app/file/' + id + '&embedded=true"></iframe><a href="javascript:void(0)">X</a></div>');
    }

    $(document).on('click', '.word-pdf-view a', function () {
        $('.word-pdf-view').remove();
    });

    $(document).on('click', '.qr-sort', function () {
        var elem = $(this).closest('.qr-txt').html();
        var cont = '<div class="qr-txt">' + elem + '</div>';
        var prev = $(this).closest('.qr-txt').prev('.qr-txt').before(cont);
        $(this).closest('.qr-txt').remove();
        var form_5 = [], form_6 = [];
        $('.jp_form_5 .jp_body .qr-txt').each(function () {
            form_5.push($(this).text());
        });
        $('.jp_form_6 .jp_body .qr-txt').each(function () {
            form_6.push($(this).text());
        });
        setTimeout(function () {
            $.ajax({
                url: '/description/update_job_posting_qr',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    id: $('#main-content').attr('data-jp'),
                    jp5: form_5,
                    jp6: form_6,
                    _token: token
                }
            });
        }, 100);
    });

    $(document).on('keyup', '#new-team-name', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            createNewProject();
        }
    });

    $(document).on('click, focusin', '#new-team-name', function (e) {
        $('.create-team-popup .popup-btn-cancel, .create-team-popup .popup-btn-create').removeClass('tab_active');
    });

    $(document).on('keydown', '#new-team-name', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 9) {
            e.preventDefault();
            $('.create-team-popup .popup-btn-cancel').addClass('tab_active').attr('first', -1).attr("tabindex", -1).focus();
        }
    });

    $(document).on('keydown', '#job-posting-name-input', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 9) {
            $('#seniority:not(.open)').click();
            $('.field-total-experience').focus();
        }
    });

    var indeX = -1;

    $(document).on('keyup', 'body', function (e) {
        e.preventDefault();
        var $items = $('.create-job-posting-popup .select-list div, .create-job-posting-popup .select-list span');
        var keyCode = e.keyCode || e.which;
        if ($('#seniority.input-field.select-type.open').length) {
            if (keyCode === 40 || keyCode === 38) {
                $items.eq(indeX).removeClass('sel');
                e.preventDefault();
                if (keyCode === 38 && indeX > 0)
                    indeX--;
                if (keyCode === 40 && indeX < $items.length - 1)
                    indeX++;
                if (!~indeX)
                    indeX = 0;
                $items.eq(indeX).addClass('sel');
            }
            else {
                indeX = -1;
            }
        }
    });

    $(document).on('keyup', 'body', function (e) {
        var keyCode = e.keyCode || e.which;
        var cancel = $('.create-team-popup .popup-btn-cancel');
        var create = $('.create-team-popup .popup-btn-create');
        if (cancel.hasClass('tab_active')) {
            if (keyCode === 39) {
                cancel.removeClass('tab_active');
                create.addClass('tab_active').attr("tabindex", -1).focus();
            }
            else if (keyCode === 9 && cancel.attr('first') == -1) {
                cancel.removeAttr('first').focus();
                return false;
            }
            else if (keyCode === 9) {
                cancel.removeClass('tab_active');
                create.addClass('tab_active').attr("tabindex", -1).focus();
            }
            else if (keyCode === 13) {
                cancel.click();
            }
        }
        else if (create.hasClass('tab_active')) {
            if (keyCode === 37) {
                create.removeClass('tab_active');
                cancel.addClass('tab_active').attr("tabindex", -1).focus();
            }
            else if (keyCode === 13) {
                create.click();
            }
        }
    });
});
