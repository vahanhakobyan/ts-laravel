(function ($) {
    "use strict";

    let token = $('meta[name="csrf-token"]').attr('content');

    $(document)
        .on('click', '#user_registration_type input', function () {
            $.ajax({
                url: "/admin/post",
                type: "post",
                data: {
                    user_registration_type: $('#user_registration_type input:checked').length,
                    action: 'update',
                    _token: token
                }
            });
        })
        .on('click', '#submit_invite_form', function () {
            let email = $('#email'),
                name = $('#name');
            if(!validateText(name)) {

            }
            else if(!validateText(email)) {

            }
            else if(!validateEmail(email)) {

            }
            else {
                $.ajax({
                    url: "/admin/post",
                    type: "post",
                    data: {
                        action: 'email_exists',
                        name: name.val(),
                        email: email.val(),
                        _token: token
                    },
                    success: function (data) {
                        if (data === 'ok') {
                            $('#invite_form').submit();
                        }
                        else {
                            let html = '';
                            html += '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">';
                            html += '    <span class="badge badge-pill badge-danger">ERROR</span>';
                            html += '    This email address is already registered.';
                            html += '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                            html += '        <span aria-hidden="true">×</span>';
                            html += '    </button>';
                            html += '</div>';
                            $('.card-body.card-block').prepend(html);
                        }
                    }
                });
            }
        });

    function validateText($this) {
        if($this.val()==='') {
            $this.focus();

            return false;
        }

        return true;
    }

    function validateEmail(email) {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email.val());
    }

})(jQuery);
