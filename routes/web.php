<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register/{token?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/all_skills', 'HomeController@all_skills');
Route::post('/home/get_list', 'HomeController@get_list');
Route::get('/profile', 'HomeController@profile');
Route::post('/profile/update', 'HomeController@update');
Route::post('/profile/old_password', 'HomeController@old_password');
Route::post('/profile/new_password', 'HomeController@new_password');

Route::resources([
    'group' => 'GroupController'
]);

Route::prefix('description')->group(function () {
    Route::post('/add', 'DescriptionController@add');
    Route::post('/edit', 'DescriptionController@edit');
    Route::post('/get', 'DescriptionController@get');
    Route::post('/update_hi', 'DescriptionController@update_hi');
    Route::post('/update_jp', 'DescriptionController@update_jp');
    Route::post('/update_job_posting_skill', 'DescriptionController@update_job_posting_skill');
    Route::post('/update_job_posting_education', 'DescriptionController@update_job_posting_education');
    Route::post('/update_job_posting_qr', 'DescriptionController@update_job_posting_qr');
    Route::post('/update_job_posting', 'DescriptionController@update_job_posting');
    Route::post('/summary_report', 'DescriptionController@summary_report');
    Route::post('/remove_job_posting', 'DescriptionController@remove_job_posting');
    Route::post('/load_select_boxes', 'DescriptionController@load_select_boxes');
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin');
    Route::any('/users', 'AdminController@users')->name('admin.users');
    Route::get('/users/settings', 'AdminController@settings')->name('admin.users.settings');
    Route::get('/users/roles', 'AdminController@roles')->name('admin.users.roles');
    Route::get('/users/role/{id?}', 'AdminController@role')->where('id', '[0-9]+');
    Route::get('/user/{id}', 'AdminController@user_edit')->where('id', '[0-9]+');
    Route::delete('/user/delete', 'AdminController@user_delete');
    Route::get('/admins', 'AdminController@admins')->name('admin.admins');
    Route::post('/post', 'AdminController@post');
    Route::get('/resumes', 'ResumeController@index');
    Route::get('/postings', 'DescriptionController@index');
	Route::get('/elastic/{keyword}', 'ResumeController@search');
});
